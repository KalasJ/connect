package com.mushhead.connect.di

import android.content.Context
import com.mushhead.connect.repository.DataRepository
import com.mushhead.connect.repository.UsersRepository
import com.mushhead.connect.repository.UsersRepositoryImpl
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DbModul {

    @Provides
    @Singleton
    fun provideDataRepository(
        @ApplicationContext context: Context
    ) = DataRepository(context = context)

    @Module
    @InstallIn(SingletonComponent::class)
    object FirebaseModule {

        @Provides
        @Singleton
        fun providesFirebaseFirestore(): FirebaseFirestore = Firebase.firestore
    }

    @Module
    @InstallIn(SingletonComponent::class)
    object FirebaseAuthModule {

        @Provides
        @Singleton
        fun providesFirebaseAuthFirestore(): FirebaseAuth = Firebase.auth
    }

    @Module
    @InstallIn(SingletonComponent::class)
    abstract class RepositoryModule {

        @Binds
        abstract fun bindsUsersRepository(usersRepositoryImpl: UsersRepositoryImpl): UsersRepository
    }

}