package com.mushhead.connect

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ConectApp : Application()