package com.mushhead.connect.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.mushhead.connect.utils.LoginScreen
import com.mushhead.connect.utils.MainScreen
import com.mushhead.connect.utils.RegisterScreen
import com.mushhead.connect.views.*
import com.google.firebase.auth.FirebaseAuth

/**
 * NavigationLogic()
 * Jedná se o funkčnost menu sestavenou podle doporučení compousu.
 * Specifikují se zde jednotlivé screeny, které se budou nacházet v menu. (Je třeba jich specifikovat tolik kolik se předává v listu v MenuDestinations!)
 * Dále je možné k jednotlivým screenam specifikovat jejich vyžadované parametry a jednoduše je tak předávat níž.
 * 27.02.2023 UPDATE
 * Pár přídavků pro novou screenu pro registraci a kdy ji aktivovat.
 */
@Composable
fun NavigationLogic(
    modifier: Modifier = Modifier,
    auth: FirebaseAuth,
    navController: NavHostController,
    startDestinations: String,
) {
    NavHost(
        navController = navController,
        startDestination =
        if (startDestinations == RegisterScreen.route) {
            startDestinations
        } else {
            if (auth.currentUser == null) {
                LoginScreen.route
            } else {
                MainScreen.route
            }
        },
        modifier = modifier
    ) {
        composable(
            route = com.mushhead.connect.utils.MapScreen.route,
            arguments = listOf(
                navArgument("lat") {
                    defaultValue = "50.073658"
                    type = NavType.StringType
                },
                navArgument("lon") {
                    defaultValue = "14.418540"
                    type = NavType.StringType
                }
            )
        ) { backStackEntry ->
            val lat = backStackEntry.arguments!!.getString("lat")
            val lon = backStackEntry.arguments!!.getString("lon")
            if (lat != null) {
                if (lon != null) {
                    MapScreen(lat = lat, lon = lon)
                }
            }
        }
        composable(route = com.mushhead.connect.utils.ProfileScreen.route) {
            ProfileScreen(
                onClickEdit = { navController.navigate(com.mushhead.connect.utils.EditScreen.route) }
            )
        }
        composable(route = MainScreen.route) {
            MainScreen(
                navController = navController
            )
        }
        composable(route = com.mushhead.connect.utils.SettingScreen.route) {
            SettingScreen()
        }
        composable(route = com.mushhead.connect.utils.AddScreen.route) {
            AddScreen(
                onClickSeeCamera = { navController.navigate(com.mushhead.connect.utils.CameraScreen.route) },
                navController = navController)
        }
        composable(route = com.mushhead.connect.utils.CameraScreen.route) {
            CameraScreen(
                modifier = modifier,
                navController = navController,
                auth = auth
            )
        }
        composable(route = RegisterScreen.route) {
            RegisterScreen(
                auth = auth,
                navHostController = navController
            )
        }
        composable(route = com.mushhead.connect.utils.EditScreen.route) {
            EditScreen(
                auth = auth,
                navController = navController
               )
        }
        composable(route = com.mushhead.connect.utils.LoginScreen.route) {
            LoginScreen(
                auth = auth,
                navController = navController
            )
        }
        composable(route = com.mushhead.connect.utils.CompleteScreen.route) {
            FinishScreen(
                navController = navController
            )
        }
    }
}

/**
 * Jedná se o pomocnou vychytralost od android developerů, které mi zapiná užitečné funkce při navigaci mezi screeny.
 */
fun NavHostController.navigateLift(route: String) =
    this.navigate(route) {
        popUpTo(
            this@navigateLift.graph.findStartDestination().id
        ) {
            saveState = true
        }
        launchSingleTop = true
        restoreState = true
    }