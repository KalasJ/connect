package com.mushhead.connect

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.mushhead.connect.components.BottomMenu
import com.mushhead.connect.components.TopBar
import com.mushhead.connect.navigation.NavigationLogic
import com.mushhead.connect.navigation.navigateLift
import com.mushhead.connect.ui.theme.ConnectTheme
import com.mushhead.connect.utils.menuTabScreens
import com.mushhead.connect.viewmodels.MainViewModel
import com.mushhead.connect.viewmodels.SplashViewModel
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject
    lateinit var splashViewModel: SplashViewModel

    //FIREBASE
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        installSplashScreen().setKeepOnScreenCondition {
            !splashViewModel.isLoading.value
        }

        setContent {
            ConnectTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val screen by splashViewModel.startDestination
                    val navController = rememberNavController()

                    auth = Firebase.auth
                    App(screen = screen,
                        auth = auth,
                        navController = navController)
                }
            }
        }
    }
}

/**
 * Základní funkce nesoucí spojení všech částí.
 * Momentálně obsahuje scaffold od material3, který mi pomáhá se grafickým vzhledem a usnaďuje mi tak práci.
 * Definuji mu vytvořený bottomBar a floatingAction Buttom
 *
 * 3.02.2023 UPDATE
 * přidal jse appbar a sprovoznil funkčnosti pro navigaci
 * 21.02.2023 UPDATE
 * Hrál jsem si se vzhledem a přidal zákaz vykreslování FloatingButtonu a menu na pár screenách.
 * Následně jsem přidal žádost o permission o lokaci, kterou potřebují získat v další screeně. (FloatingButton)
 * 27.02.2023 UPDATE
 * Přidal jsem funckionalutu pro schovanání topAppBaru při registraci.
 * Přidal jsem funkcionality pro registrační screen
 */
@OptIn(ExperimentalMaterial3Api::class, ExperimentalPermissionsApi::class)
@Composable
fun App(
    screen: String,
    auth: FirebaseAuth,
    modifier: Modifier = Modifier,
    navController: NavHostController,
    mainViewModel: MainViewModel = hiltViewModel(),
) {
    //NAVIGACE
    val currentBackStack by navController.currentBackStackEntryAsState()
    val currentDestination = currentBackStack?.destination
    val currentScreen =
        menuTabScreens.find { it.route == currentDestination?.route }
            ?: com.mushhead.connect.utils.MainScreen


    //DOČASNÉ schování bottomMenu na určitých screenách, ale budu to muset udělat lépe
    var showBottomBar by rememberSaveable { mutableStateOf(true) }
    showBottomBar = when (currentBackStack?.destination?.route) {
        "Camera" -> false // on this screen bottom bar should be hidden
        "Settings" -> false
        "Register" -> false
        "Add" -> false
        "Edit" -> false
        "Login" -> false
        "Complete" -> false
        else -> true // in all other cases show bottom bar
    }

    //DOČASNÉ schování floatingButtonu na určitých screenách, ale budu to muset udělat lépe
    var showFloatingButton by rememberSaveable { mutableStateOf(true) }
    showFloatingButton = when (currentBackStack?.destination?.route) {
        "Profile" -> false
        "Add" -> false
        "Map?lat={lat}&lot={lon}" -> false
        "Register" -> false
        "Camera" -> false
        "Edit" -> false
        "Login" -> false
        "Complete" -> false
        else -> true
    }

    //DOČASNÉ schování topBaru na určitých screenách, ale budu to muset udělat lépe
    var showTopAppBar by rememberSaveable { mutableStateOf(true) }
    showTopAppBar = when (currentBackStack?.destination?.route) {
        "Register" -> false
        "Camera" -> false
        "Login" -> false
        "Complete" -> false
        else -> true
    }


    if (screen == "Register") {
        NavigationLogic(navController = navController,
            startDestinations = screen,
            auth = auth
        )
    } else {
        Scaffold(
            containerColor = MaterialTheme.colorScheme.surface,
            contentColor = MaterialTheme.colorScheme.onSurface,
            topBar = {
                if (showTopAppBar) {
                    TopBar(
                        modifier = modifier,
                        navController = navController,
                        onTabSelected = { newScreen ->
                            navController.navigate(newScreen.route)
                        },
                        screenName = currentBackStack?.destination?.route.toString(),
                        showMenu = currentBackStack?.destination?.route != "Home",
                        showBackArrow =
                            if(currentBackStack?.destination?.route != "Map?lat={lat}&lot={lon}"){
                                currentBackStack?.destination?.route != "Profile"
                            }else{
                               false
                            },
                        auth = auth
                    )
                }
            },
            floatingActionButton = {
                if (showBottomBar) {
                    if (showFloatingButton) {
                        FloatingActionButton(
                            shape = MaterialTheme.shapes.large,
                            containerColor = MaterialTheme.colorScheme.primary,
                            onClick = {
                                navController.navigate(com.mushhead.connect.utils.AddScreen.route)
                            },
                            elevation = FloatingActionButtonDefaults.bottomAppBarFabElevation(
                                defaultElevation = 3.dp)
                        ) {
                            Icon(
                                Icons.Filled.Add,
                                stringResource(id = R.string.Float_text),
                                tint = MaterialTheme.colorScheme.onPrimary
                            )
                        }
                    }
                }
            },
            bottomBar = {
                if (showBottomBar) {
                    BottomMenu(
                        modifier = modifier,
                        allScreens = menuTabScreens,
                        onTabSelected = { newScreen ->
                            navController.navigateLift(newScreen.route)
                        },
                        currentScreen = currentScreen
                    )
                }
            }
        ) { innerPadding ->
            Column() {
                /*
                Text(
                    modifier = Modifier.padding(innerPadding),
                text = auth.currentUser!!.email!!)

                 */
                NavigationLogic(
                    modifier = Modifier.padding(innerPadding),
                    navController = navController,
                    startDestinations = screen,
                    auth = auth
                )
            }
        }
    }
}


