package com.mushhead.connect.repository

import android.util.Log
import com.mushhead.connect.utils.Friend
import com.mushhead.connect.utils.Resource
import com.mushhead.connect.utils.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject

class UsersRepositoryImpl @Inject constructor(
    private val firebaseFirestore: FirebaseFirestore,
    private val firebaseAuth: FirebaseAuth,
) : UsersRepository {

    override fun getOwner(): Flow<Resource<List<User>>> = callbackFlow {
        val auth = firebaseAuth
        // Creating a reference to the cars collection
        val docRef =
            firebaseFirestore.collection("users").whereEqualTo("mail", auth.currentUser!!.email!!)

        // Get the data
        docRef.get()
            .addOnSuccessListener { result ->
                // Converts the result data to our List<Users>
                val users = result.toObjects<User>()
                // Emits the data
                trySend(Resource.Success(data = users)).isSuccess
            }
            .addOnFailureListener {
                /* TODO: Handle the error */
            }

        awaitClose {
            close()
        }
    }
    override fun getOwnerRealTime(): Flow<Resource<List<User>>> = callbackFlow {
        val auth = firebaseAuth
        // Creating a reference to the cars collection
        val docRef =
            firebaseFirestore.collection("users").whereEqualTo("mail", auth.currentUser!!.email!!)

        // Listen to data real-time
        val listener = docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                /* TODO: Handle the error */
                return@addSnapshotListener
            }

            if (snapshot != null) {
                // Converts the result data to our List<Users>
                val users = snapshot.toObjects<User>()

                // Emits the data
                trySend(Resource.Success(data = users)).isSuccess
            }
        }

        awaitClose {
            // Remove the database listener
            listener.remove()
            close()
        }
    }

    override fun getAllUsers(): Flow<Resource<List<User>>> = callbackFlow {
        // Creating a reference to the cars collection
        val docRef = firebaseFirestore.collection("users")

        // Get the data
        docRef.get()
            .addOnSuccessListener { result ->
                // Converts the result data to our List<Car>
                val users = result.toObjects<User>()

                // Emits the data
                trySend(Resource.Success(data = users)).isSuccess
            }
            .addOnFailureListener {
                /* TODO: Handle the error */
            }

        awaitClose {
            close()
        }
    }
    override fun getAllUsersRealTime(): Flow<Resource<List<User>>> = callbackFlow {
        // Creating a reference to the cars collection
        val docRef = firebaseFirestore.collection("users")

        // Listen to data real-time
        val listener = docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                /* TODO: Handle the error */
                return@addSnapshotListener
            }

            if (snapshot != null) {
                // Converts the result data to our List<Users>
                val users = snapshot.toObjects<User>()

                // Emits the data
                trySend(Resource.Success(data = users)).isSuccess
            }
        }

        awaitClose {
            // Remove the database listener
            listener.remove()
            close()
        }
    }

    override fun getOwnersFriends(mail: Map<String, Friend>): Flow<Resource<List<User>>> = callbackFlow {
        Log.d("FRIENDS", "${mail.size}")
        val friends = mutableListOf<User>()
        for (mails in mail) {
            if (mails.key != "e"){
                Log.d("FRIENDS", mails.value.mail)
                val docRef = firebaseFirestore.collection("users").document(mails.value.mail)
                // Get the data
                docRef.get()
                    .addOnSuccessListener { result ->
                        val user = result.toObject<User>()
                        if (user != null) {
                            //Log.d("FRIENDS", "$user")
                            friends.add(user)
                            trySend(Resource.Success(data = friends)).isSuccess
                        }
                    }
                    .addOnFailureListener {
                        /* TODO: Handle the error */
                    }
            }
        }

        awaitClose {
            close()
        }
    }

    override fun deleteUser(mail: String) {
        val auth = firebaseAuth

        val deleteFriend: MutableMap<String, Any> = HashMap()
        deleteFriend["friends.${mail.replace(".","-")}"] = FieldValue.delete();


        val docRef = firebaseFirestore.collection("users").document(auth.currentUser!!.email!!)
        docRef.update(deleteFriend)
    }

    override fun updateSettings(setting: String, option: Boolean) {
        val auth = firebaseAuth

        val updateSettings: MutableMap<String, Any> = HashMap()
        updateSettings["settings.${setting}"] = option

        val docRef = firebaseFirestore.collection("users").document(auth.currentUser!!.email!!)
        docRef.update(updateSettings)
    }
}