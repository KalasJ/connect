package com.mushhead.connect.repository

import com.mushhead.connect.utils.Friend
import com.mushhead.connect.utils.Resource
import com.mushhead.connect.utils.User
import kotlinx.coroutines.flow.Flow

interface UsersRepository {
    fun getOwner(): Flow<Resource<List<User>>>
    fun getOwnerRealTime(): Flow<Resource<List<User>>>

    fun getAllUsers(): Flow<Resource<List<User>>>
    fun getAllUsersRealTime(): Flow<Resource<List<User>>>

    fun getOwnersFriends(mail: Map<String, Friend>): Flow<Resource<List<User>>>

    fun deleteUser(mail: String)
    fun updateSettings(setting: String, option: Boolean)
}