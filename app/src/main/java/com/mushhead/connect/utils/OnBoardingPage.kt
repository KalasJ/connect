package com.mushhead.connect.utils

import androidx.annotation.DrawableRes
import com.mushhead.connect.R

sealed class OnBoardingPage(
    @DrawableRes
    val image: Int,
    val title: Int,
    val description: Int
) {
    object First : OnBoardingPage(
        image = R.drawable.onboard_1,
        title = R.string.onBoard_label1,
        description = R.string.onBoard_label_des1
    )

    object Second : OnBoardingPage(
        image = R.drawable.onboard_2,
        title = R.string.onBoard_label2,
        description = R.string.onBoard_label_des2,
    )

    object Third : OnBoardingPage(
        image = R.drawable.login_no_background,
        title = R.string.onBoard_label3,
        description = R.string.log_desc,
    )
}