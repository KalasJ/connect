package com.mushhead.connect.utils

data class User(
    val fullname: String = "",
    var mobile: String = "",
    var mail: String = "",
    var facebook: String = "",
    var instagram: String = "",
    var twitter: String = "",
    var friends: Map<String,Friend> = emptyMap(),
    var settings: Map<String,Boolean> = emptyMap(),
)