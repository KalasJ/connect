package com.mushhead.connect.utils

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.ui.graphics.vector.ImageVector

/**
 * Jedná se o součást navigace, slouží pro zjednodušení a zpřehlednění kodu.
 */
interface MenuDestinations {
    val icon: ImageVector
    val route: String
}


object MainScreen : MenuDestinations {
    override val icon = Icons.Outlined.Home
    override val route = "Home"
}

object MapScreen : MenuDestinations {
    override val icon = Icons.Outlined.Map
    override val route = "Map?lat={lat}&lot={lon}"
}

object ProfileScreen : MenuDestinations {
    override val icon = Icons.Outlined.PersonOutline
    override val route = "Profile"
}

object SettingScreen : MenuDestinations {
    override val icon = Icons.Outlined.Settings
    override val route = "Settings"
}

object AddScreen : MenuDestinations {
    override val icon = Icons.Outlined.Add
    override val route = "Add"
}

object CameraScreen : MenuDestinations {
    override val icon = Icons.Outlined.Camera
    override val route = "Camera"
}

object RegisterScreen : MenuDestinations {
    override val icon = Icons.Outlined.AppRegistration
    override val route = "Register"
}

object EditScreen : MenuDestinations {
    override val icon = Icons.Outlined.Edit
    override val route = "Edit"
}

object LoginScreen : MenuDestinations {
    override val icon = Icons.Outlined.Login
    override val route = "Login"
}

object CompleteScreen : MenuDestinations {
    override val icon = Icons.Outlined.IncompleteCircle
    override val route = "Complete"
}

var menuTabScreens = listOf(MainScreen, MapScreen, ProfileScreen)