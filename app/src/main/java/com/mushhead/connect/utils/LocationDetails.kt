package com.mushhead.connect.utils

data class LocationDetails(var lat: Double, var lon: Double)
