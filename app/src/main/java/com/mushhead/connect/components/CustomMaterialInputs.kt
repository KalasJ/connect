package com.mushhead.connect.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Error
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InputRow(
    modifier: Modifier,
    value: String,
    onValueChange: (String) -> Unit,
    label: String,
    placeHolder: String,
    supporting: String = "",
    singleLine: Boolean = true,
) {
    var isError by remember { mutableStateOf(false) }
    fun validate(text: String) {
        isError = text.isEmpty()
    }

    OutlinedTextField(
        modifier = modifier
            .width(400.dp),
        value = value,
        onValueChange = {
            onValueChange(it)
            validate(it)
        },
        label = {
            Text(
                fontStyle = MaterialTheme.typography.bodySmall.fontStyle,
                fontWeight = MaterialTheme.typography.bodySmall.fontWeight,
                fontSize = MaterialTheme.typography.bodySmall.fontSize,
                fontFamily = MaterialTheme.typography.bodySmall.fontFamily,
                text = label
            )
        },
        isError = isError,
        placeholder = { Text(placeHolder) },
        singleLine = singleLine,
        supportingText =
        {
            if (isError) {
                Text(
                    fontStyle = MaterialTheme.typography.labelMedium.fontStyle,
                    fontWeight = MaterialTheme.typography.labelMedium.fontWeight,
                    fontSize = MaterialTheme.typography.labelMedium.fontSize,
                    modifier = modifier
                        .fillMaxWidth(),
                    color = MaterialTheme.colorScheme.error,
                    text = supporting
                )
            }
        },
        trailingIcon = {
            if (isError)
                Icon(Icons.Filled.Error, "error", tint = MaterialTheme.colorScheme.error)
        },
        keyboardActions = KeyboardActions { validate(value) },
        shape = MaterialTheme.shapes.extraSmall,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            textColor = MaterialTheme.colorScheme.onSurfaceVariant,
        )
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InputRowEditScreen(
    modifier: Modifier,
    value: String,
    onValueChange: (String) -> Unit,
    label: String,
    placeHolder: String,
    singleLine: Boolean = true,
) {
    OutlinedTextField(
        modifier = modifier
            .width(400.dp),
        value = value,
        onValueChange = {
            onValueChange(it)
        },
        label = {
            Text(
                fontStyle = MaterialTheme.typography.bodySmall.fontStyle,
                fontWeight = MaterialTheme.typography.bodySmall.fontWeight,
                fontSize = MaterialTheme.typography.bodySmall.fontSize,
                fontFamily = MaterialTheme.typography.bodySmall.fontFamily,
                text = label
            )
        },
        placeholder = { Text(placeHolder) },
        singleLine = singleLine,
        shape = MaterialTheme.shapes.extraSmall,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            textColor = MaterialTheme.colorScheme.onSurfaceVariant,
        )
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InputRowPasswordReg(
    modifier: Modifier,
    value: String,
    onValueChange: (String) -> Unit,
    label: String,
    placeHolder: String,
    supporting: String = "",
    singleLine: Boolean = true,
) {
    var isError by remember { mutableStateOf(false) }
    var passwordVisible by remember { mutableStateOf(false) }

    fun validate(text: String) {
        if(text != ""){
            isError = text.length < 3 && text.isNotEmpty()
        }else{
            isError = true
        }
    }

    OutlinedTextField(
        modifier = modifier
            .width(400.dp),
        value = value,
        onValueChange = {
            onValueChange(it)
            validate(it)
        },
        visualTransformation =
        if (passwordVisible) {
            VisualTransformation.None
        } else {
            PasswordVisualTransformation()
        },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
        trailingIcon = {
            val image = if (passwordVisible)
                Icons.Filled.Visibility
            else Icons.Filled.VisibilityOff

            // Please provide localized description for accessibility services
            val description = if (passwordVisible) "Hide password" else "Show password"

            IconButton(onClick = { passwordVisible = !passwordVisible }) {
                Icon(imageVector = image, description)
            }
        },
        label = {
            Text(
                fontStyle = MaterialTheme.typography.bodySmall.fontStyle,
                fontWeight = MaterialTheme.typography.bodySmall.fontWeight,
                fontSize = MaterialTheme.typography.bodySmall.fontSize,
                fontFamily = MaterialTheme.typography.bodySmall.fontFamily,
                text = label
            )
        },
        placeholder = { Text(placeHolder) },
        singleLine = singleLine,
        supportingText =
        {
            if (isError) {
                Text(
                    fontStyle = MaterialTheme.typography.labelMedium.fontStyle,
                    fontWeight = MaterialTheme.typography.labelMedium.fontWeight,
                    fontSize = MaterialTheme.typography.labelMedium.fontSize,
                    modifier = modifier
                        .fillMaxWidth(),
                    color = MaterialTheme.colorScheme.error,
                    text = supporting
                )
            }
        },
        keyboardActions = KeyboardActions { validate(value) },
        shape = MaterialTheme.shapes.extraSmall,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            textColor = MaterialTheme.colorScheme.onSurfaceVariant)
    )
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InputRowPasswordLog(
    modifier: Modifier,
    value: String,
    onValueChange: (String) -> Unit,
    label: String,
    placeHolder: String,
    supporting: String = "",
    singleLine: Boolean = true,
) {
    var isError by remember { mutableStateOf(false) }
    var passwordVisible by remember { mutableStateOf(false) }

    fun validate(text: String) {
        isError = text == ""
    }

    OutlinedTextField(
        modifier = modifier
            .width(400.dp),
        value = value,
        onValueChange = {
            onValueChange(it)
            validate(it)
        },
        visualTransformation =
        if (passwordVisible) {
            VisualTransformation.None
        } else {
            PasswordVisualTransformation()
        },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
        trailingIcon = {
            val image = if (passwordVisible)
                Icons.Filled.Visibility
            else Icons.Filled.VisibilityOff

            // Please provide localized description for accessibility services
            val description = if (passwordVisible) "Hide password" else "Show password"

            IconButton(onClick = { passwordVisible = !passwordVisible }) {
                Icon(imageVector = image, description)
            }
        },
        label = {
            Text(
                fontStyle = MaterialTheme.typography.bodySmall.fontStyle,
                fontWeight = MaterialTheme.typography.bodySmall.fontWeight,
                fontSize = MaterialTheme.typography.bodySmall.fontSize,
                fontFamily = MaterialTheme.typography.bodySmall.fontFamily,
                text = label
            )
        },
        placeholder = { Text(placeHolder) },
        singleLine = singleLine,
        supportingText =
        {
            if (isError) {
                Text(
                    fontStyle = MaterialTheme.typography.labelMedium.fontStyle,
                    fontWeight = MaterialTheme.typography.labelMedium.fontWeight,
                    fontSize = MaterialTheme.typography.labelMedium.fontSize,
                    modifier = modifier
                        .fillMaxWidth(),
                    color = MaterialTheme.colorScheme.error,
                    text = supporting
                )
            }
        },
        keyboardActions = KeyboardActions { validate(value) },
        shape = MaterialTheme.shapes.extraSmall,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            textColor = MaterialTheme.colorScheme.onSurfaceVariant)
    )
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InputRowMobile(
    modifier: Modifier,
    value: String,
    onValueChange: (String) -> Unit,
    label: String,
    placeHolder: String,
    supporting: String = "",
    singleLine: Boolean = true,
) {
    OutlinedTextField(
        modifier = modifier
            .width(400.dp),
        value = value,
        onValueChange = onValueChange,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Phone),
        label = {
            Text(
                fontStyle = MaterialTheme.typography.bodySmall.fontStyle,
                fontWeight = MaterialTheme.typography.bodySmall.fontWeight,
                fontSize = MaterialTheme.typography.bodySmall.fontSize,
                fontFamily = MaterialTheme.typography.bodySmall.fontFamily,
                text = label
            )
        },
        placeholder = { Text(placeHolder) },
        singleLine = singleLine,
        supportingText =
        {
            Text(
                fontStyle = MaterialTheme.typography.labelMedium.fontStyle,
                fontWeight = MaterialTheme.typography.labelMedium.fontWeight,
                fontSize = MaterialTheme.typography.labelMedium.fontSize,
                modifier = modifier
                    .fillMaxWidth(),
                color = MaterialTheme.colorScheme.error,
                text = supporting
            )
        },
        shape = MaterialTheme.shapes.extraSmall,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            textColor = MaterialTheme.colorScheme.onSurfaceVariant,
        )
    )
}