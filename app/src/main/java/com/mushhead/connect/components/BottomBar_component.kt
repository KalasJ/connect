package com.mushhead.connect.components

import androidx.compose.foundation.layout.height
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.mushhead.connect.utils.MenuDestinations

/**
 * Komponenta která dává vzhled mému menu.
 * Využívám předefinované menu od material3, abych zajistil jistotu, že vzhled bude odpovídat všem normám
 * Vyplnuji jen potřebné parametry právě z MenuDestination a předávám lambdy
 */
@Composable
fun BottomMenu(
    modifier: Modifier = Modifier,
    allScreens: List<MenuDestinations>,
    onTabSelected: (MenuDestinations) -> Unit,
    currentScreen: MenuDestinations,
) {
    BottomAppBar(
        modifier = modifier
            .height(50.dp)
            .shadow(
                elevation = 6.dp,
                clip = true),
        containerColor = MaterialTheme.colorScheme.surfaceVariant,
        contentColor = MaterialTheme.colorScheme.onSurface
    ) {
        allScreens.forEachIndexed { _, item ->
            NavigationBarItem(
                icon = { Icon(imageVector = item.icon, contentDescription = item.route) },
                selected = currentScreen == item,
                onClick = { onTabSelected(item) },
                colors = NavigationBarItemDefaults.colors(
                    selectedIconColor = MaterialTheme.colorScheme.onSurfaceVariant,
                    selectedTextColor = MaterialTheme.colorScheme.onPrimary,
                    indicatorColor = MaterialTheme.colorScheme.onPrimary
                )
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun BottomMenuPrew() {
    MaterialTheme {

    }
}