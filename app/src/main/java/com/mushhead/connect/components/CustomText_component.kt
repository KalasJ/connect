package com.mushhead.connect.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.PersonOutline
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp


@Composable
fun CustomText(
    modifier: Modifier = Modifier,
    icon: ImageVector,
    headline: String,
    text: String
) {
    Column() {
        Row(
            modifier = modifier
                .fillMaxWidth()
                .height(60.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                modifier = modifier
                    .weight(1f)
                    .fillMaxSize(),
                imageVector = icon,
                contentDescription = null)
            Spacer(modifier = modifier.size(10.dp))
            Column(
                modifier = modifier
                    .weight(9f)
            ) {
                Text(
                    text = headline,
                    style = MaterialTheme.typography.bodyLarge,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    text = text,
                    style = MaterialTheme.typography.bodySmall,
                )
            }
        }
        Divider(thickness = 0.5.dp, color = Color.Gray)
    }
}

@Preview(showBackground = true)
@Composable
fun Test() {
    MaterialTheme {
        CustomText(icon = Icons.Outlined.PersonOutline ,headline = "Name", text = "ASD")
    }
}