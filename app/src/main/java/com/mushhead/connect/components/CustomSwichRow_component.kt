package com.mushhead.connect.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.Icon
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.dp

@Composable
fun SwichRow(
    modifier: Modifier,
    lambda: () -> Unit,
    checked: Boolean,
    swichHeadline: Int,
    swichDescriptor: Int,
) {
    val des = stringResource(id = swichDescriptor)
    val icon: (@Composable () -> Unit)? = if (checked) {
        {
            Icon(
                imageVector = Icons.Filled.Check,
                contentDescription = null,
                modifier = Modifier.size(SwitchDefaults.IconSize),
            )
        }
    } else {
        null
    }

    Row(
        modifier = modifier
            .wrapContentSize(),
    ) {
        Text(
            modifier = Modifier
                .align(Alignment.CenterVertically),
            text = stringResource(id = swichHeadline)
        )
        Spacer(modifier = Modifier.weight(1f))
        Switch(
            modifier = Modifier
                .semantics { contentDescription = des }
                .alignByBaseline(),
            checked = checked,
            onCheckedChange = {
                lambda()
            },
            thumbContent = icon
        )
    }
    androidx.compose.material.Divider(
        modifier = modifier
            .padding(5.dp),
        thickness = 0.5.dp,
        color = Color.Gray
    )
}