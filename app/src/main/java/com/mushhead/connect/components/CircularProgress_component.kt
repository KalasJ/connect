package com.mushhead.connect.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp


@Composable
fun CircularProgress(
    modifier: Modifier,
    isDisplayed: Boolean,
) {
    if (isDisplayed) {
        Box(
            modifier = modifier
                .wrapContentSize()
                .size(100.dp)
        ) {
            CircularProgressIndicator(
                modifier = modifier
                    .size(120.dp)
                    .align(Alignment.Center),
                color = MaterialTheme.colorScheme.primary,
                strokeWidth = 10.dp
            )
            CircularProgressIndicator(
                modifier = modifier
                    .size(100.dp),
                color = MaterialTheme.colorScheme.inversePrimary,
                strokeWidth = 7.dp
            )
        }
    }
}