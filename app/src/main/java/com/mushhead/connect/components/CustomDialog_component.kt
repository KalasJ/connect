package com.mushhead.connect.components

import com.mushhead.connect.R
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp

@Composable
fun CustomDialog(
    title: String,
    content: String,
    onDismis: () -> Unit,
    onAccept: () -> Unit,
    changeStatus: () -> Unit
){
    Surface(
        modifier = Modifier
            .padding(start = 20.dp, end = 20.dp),
        color = MaterialTheme.colorScheme.surfaceVariant.copy(alpha = 0.8f),
        contentColor = MaterialTheme.colorScheme.onSurface,
        shadowElevation = 5.dp,
        shape = MaterialTheme.shapes.large,
        border = BorderStroke(0.5.dp, MaterialTheme.colorScheme.onSurface)
    ){
        AlertDialog(
            onDismissRequest = {
                changeStatus()
            },
            title = {
                Text(text = title)
            },
            text = {
                Text(text = content)
            },
            confirmButton = {
                TextButton(
                    onClick = {
                        onAccept()
                        changeStatus()
                    }
                ) {
                    Text(stringResource(R.string.custom_dialog_accept_logout_click))
                }
            },
            dismissButton = {
                TextButton(
                    onClick = {
                        onDismis()
                        changeStatus()
                    }
                ) {
                    Text(stringResource(R.string.custom_dialog_dismis_click))
                }
            }
        )
    }
}