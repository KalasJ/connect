package com.mushhead.connect.components

import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp


@Composable
fun InputText(
    modifier: Modifier,
    value: String,
    onValue: (String) -> Unit,
    label: String,
    placeHolder: String,
    supportingNull: String
) {
    InputRow(
        modifier = modifier
            .padding(1.dp),
        value = value,
        singleLine = true,
        onValueChange = { onValue(it) },
        label = label,
        placeHolder = placeHolder,
        supporting = supportingNull
    )
}

@Composable
fun InputPasswordReg(
    modifier: Modifier,
    value: String,
    onValue: (String) -> Unit,
    label: String,
    placeHolder: String,
    supportingNull: String,
    supportingToSmall: String,
) {
    InputRowPasswordReg(
        modifier = modifier
            .padding(1.dp),
        value = value,
        singleLine = true,
        onValueChange = { onValue(it) },
        label = label,
        placeHolder = placeHolder,
        supporting =
        if (value.isEmpty()) {
            supportingNull
        } else {
            if(value.length < 3){
                supportingToSmall
            }else{
                ""
            }
        }
    )
}

@Composable
fun InputPasswordLog(
    modifier: Modifier,
    value: String,
    onValue: (String) -> Unit,
    label: String,
    placeHolder: String,
    supportingNull: String,
) {
    InputRowPasswordLog(
        modifier = modifier
            .padding(1.dp),
        value = value,
        singleLine = true,
        onValueChange = { onValue(it) },
        label = label,
        placeHolder = placeHolder,
        supporting = supportingNull
    )
}