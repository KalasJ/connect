package com.mushhead.connect.components

import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ArrowBack
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.mushhead.connect.R
import com.mushhead.connect.utils.MenuDestinations
import com.google.firebase.auth.FirebaseAuth


/**
 * Komponent pro implementaci Appbaru s možnostmi otevřít si horní menu s nastavením + šipka pro navrácení
 * Třeba se v budoucnu podívat jak nastavit to vracení zpět pomocí backstacku, protože mě to vždy vrátí na Home, ale hádám že řešení naleznu až budu implementovat jednotlivé screeny.
 *
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar(
    modifier: Modifier = Modifier,
    navController: NavController,
    onTabSelected: (MenuDestinations) -> Unit,
    showMenu: Boolean = true,
    showBackArrow: Boolean = true,
    auth: FirebaseAuth,
    screenName: String,
){
    TopAppBar(
        modifier = modifier
            .shadow(
                elevation = 3.dp,
                clip = true),
        colors = TopAppBarDefaults.smallTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.surfaceVariant,
        ),
        title = {
            Text(
                text =
                if(screenName == "Home"){
                    "Connect"
                }else{
                    when(screenName == "Map?lat={lat}&lot={lon}"){
                        true -> stringResource(id = R.string.menu_destination_map)
                        false -> when(screenName == "Profile") {
                            true -> stringResource(id = R.string.menu_destination_profile)
                            false -> when(screenName == "Settings") {
                                true -> stringResource(id = R.string.menu_destination_settings)
                                false -> when(screenName == "Add") {
                                    true -> stringResource(id = R.string.menu_destination_add)
                                    false -> when(screenName == "Camera") {
                                        true -> stringResource(id = R.string.menu_destination_camera)
                                        false -> when(screenName == "Edit") {
                                            true -> stringResource(id = R.string.menu_destination_edit)
                                            false -> when(screenName == "Login") {
                                                true -> stringResource(id = R.string.menu_destination_login)
                                                false -> when(screenName == "Register") {
                                                    true -> stringResource(id = R.string.menu_destination_register)
                                                    false -> screenName
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                color = MaterialTheme.colorScheme.onSurfaceVariant,
                fontFamily = MaterialTheme.typography.titleLarge.fontFamily,
                fontWeight = MaterialTheme.typography.titleLarge.fontWeight,
                lineHeight = MaterialTheme.typography.titleLarge.lineHeight,
                fontSize = MaterialTheme.typography.titleLarge.fontSize,
            )
        },
        actions = {
            if(showMenu){
                Column(modifier = modifier){
                    var showTopMenu by rememberSaveable { mutableStateOf(false) }

                    if(screenName != "Settings"){
                        IconButton(onClick = {showTopMenu = !showTopMenu}) {
                            Icon(
                                imageVector = Icons.Outlined.MoreVert,
                                contentDescription = "Top menu"
                            )
                        }
                        TopBarMenu(
                            onTabSelected, showTopMenu,
                            onMenuClose = {showTopMenu = false},
                            auth = auth,
                            navController = navController,
                            screenName = screenName,
                        )
                    }
                }
            }
        },
        navigationIcon = {
            if(showBackArrow){
                if (navController.previousBackStackEntry != null) {
                    IconButton(
                        colors = IconButtonDefaults.iconButtonColors(
                        ),
                        onClick = { navController.popBackStack()}) {
                        Icon(
                            imageVector = Icons.Outlined.ArrowBack,
                            contentDescription = "To previous screen"
                        )
                    }
                } else {
                    null
                }
            }
        }
    )
}

@Preview(showBackground = true)
@Composable
fun TopAppBarPrew() {
    MaterialTheme {

    }
}