package com.mushhead.connect.components

import com.mushhead.connect.R
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Logout
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.mushhead.connect.utils.LoginScreen
import com.mushhead.connect.utils.MenuDestinations
import com.mushhead.connect.utils.SettingScreen
import com.google.firebase.auth.FirebaseAuth

/**
 * Otevření menu po kliknutí na iconu tří teček
 * Rozdělil jsem AppBar do dvou částí pro lepší spravování
 */
@Composable
fun TopBarMenu(
    onTabSelected: (MenuDestinations) -> Unit,
    showTopMenu: Boolean,
    auth: FirebaseAuth,
    navController: NavController,
    screenName: String,
    onMenuClose: () -> Unit,
) {
    val openDialog = remember { mutableStateOf(false) }

    if (openDialog.value) {
        CustomDialog(
            title = stringResource(id = R.string.custom_dialog_lab_logout),
            content = stringResource(id = R.string.custom_dialog_main_logout),
            onDismis = { /*TODO*/ },
            onAccept = {
                auth.signOut()
                navController.popBackStack()
                navController.navigate(LoginScreen.route)
            },
            changeStatus = { openDialog.value = !openDialog.value }
        )
    }

    DropdownMenu(
        expanded = showTopMenu,
        onDismissRequest = onMenuClose
    ) {
        DropdownMenuItem(
            text = { Text(stringResource(R.string.TopMenu_settings)) },
            onClick = {
                onTabSelected(SettingScreen)
                onMenuClose()
            },
            leadingIcon = {
                Icon(
                    Icons.Outlined.Settings,
                    contentDescription = stringResource(R.string.TopMenu_settings)
                )
            }
        )
        if (screenName != "Edit") {
            DropdownMenuItem(
                text = { Text(stringResource(R.string.Topmenu_logout)) },
                onClick = {
                    onMenuClose()
                    openDialog.value = !openDialog.value
                },
                leadingIcon = {
                    Icon(
                        Icons.Outlined.Logout,
                        contentDescription = stringResource(R.string.TopMenu_settings)
                    )
                }
            )
        }
    }
}

