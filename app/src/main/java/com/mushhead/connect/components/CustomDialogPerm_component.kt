package com.mushhead.connect.components

import com.mushhead.connect.R
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp

/**
 * 21.02.2023
 * Šikovný dialog který můžu použít všudemožně při lehké změně atributů
 */
@Composable
fun CustomDialogPermission(changeStatus: () -> Unit) {
    Surface(
        modifier = Modifier
            .padding(start = 20.dp, end = 20.dp),
        color = MaterialTheme.colorScheme.surfaceVariant.copy(alpha = 0.8f),
        contentColor = MaterialTheme.colorScheme.onSurface,
        shadowElevation = 5.dp,
        shape = MaterialTheme.shapes.large,
        border = BorderStroke(0.5.dp, MaterialTheme.colorScheme.onSurface)
    ) {
        AlertDialog(
            onDismissRequest = {
                changeStatus()
            },
            title = {
                Text(
                    fontStyle = MaterialTheme.typography.titleLarge.fontStyle,
                    fontWeight = MaterialTheme.typography.titleLarge.fontWeight,
                    fontSize = MaterialTheme.typography.titleLarge.fontSize,
                    text = stringResource(R.string.custom_dialog_lab)
                )
            },
            text = {
                Text(
                    fontStyle = MaterialTheme.typography.labelLarge.fontStyle,
                    fontWeight = MaterialTheme.typography.labelLarge.fontWeight,
                    fontSize = MaterialTheme.typography.labelLarge.fontSize,
                    text = stringResource(R.string.custom_dialog_main)
                )
            },
            confirmButton = {
                TextButton(
                    onClick = {
                        changeStatus()
                    }
                ) {
                    Text(stringResource(R.string.custom_dialog_accept_click))
                }
            }
        )
    }
}