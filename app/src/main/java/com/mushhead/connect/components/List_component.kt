package com.mushhead.connect.components


import androidx.compose.animation.*
import androidx.compose.animation.core.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.ContentAlpha
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Alignment.Companion.Start
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import com.mushhead.connect.R
import com.mushhead.connect.utils.Friend
import com.mushhead.connect.utils.User
import com.mushhead.connect.viewmodels.UsersViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*


@Composable
fun StateList(
    modifier: Modifier = Modifier,
    topHeadLine: String,
    navController: NavHostController,
    usersViewModel: UsersViewModel = hiltViewModel(),
) {
    var empty by remember { mutableStateOf(false) }
    var loading by remember { mutableStateOf(false) }
    val scope = rememberCoroutineScope()

    scope.launch {
        delay(1000)
        empty = usersViewModel.empty.value
    }
    loading = usersViewModel.loading.value



    LazyListWithHeadLine(
        modifier = modifier,
        topHeadLine = topHeadLine,
        navController = navController,
        usersViewModel = usersViewModel,
        friendList = usersViewModel.friends.value,
        empty = empty,
        loading = loading,
        locationFriendsMap = usersViewModel.owner.value.friends
    )
}

/**
 * 08.02.2023
 * Konečně se mi podařilo naimplementovat ten viewModel.. chyba byla v tom, že jej nelze definovat jako parametr ve funkci.
 * Po dlouhém bádání jsem konečně narazil na odpověď a tou je buď inicialaizovat ten viewModel hned v Activitě a poslat si jej od tud a nebo přidat dependenci pro hilt-navigaci a to umožňí využít "hiltViewModel<XXXXViewModel>()" který způsobí
 * to že mi hilt poskytne viewModel tam kde jej potřebuji.
 * 23.02.2023 UPDATE
 * Již funguje normální přidávání uživatelů, není třeba tlačítka
 */
@Composable
fun LazyListWithHeadLine(
    modifier: Modifier,
    topHeadLine: String,
    navController: NavHostController,
    friendList: List<User>,
    usersViewModel: UsersViewModel,
    locationFriendsMap: Map<String, Friend>,
    empty: Boolean,
    loading: Boolean,
) {
    Column(
        modifier = modifier
            .fillMaxSize(),
        horizontalAlignment = Start,
        verticalArrangement = Arrangement.Top
    ) {
        Text(
            modifier = modifier
                .padding(5.dp),
            text = topHeadLine,
            fontStyle = MaterialTheme.typography.labelLarge.fontStyle,
            fontWeight = MaterialTheme.typography.labelLarge.fontWeight,
            fontSize = MaterialTheme.typography.labelLarge.fontSize,
        )
        AnimatedVisibility(visible = loading) {
            Column(
                modifier = modifier
                    .fillMaxSize(),
                horizontalAlignment = CenterHorizontally,
                verticalArrangement = Arrangement.Center
            )
            {
                CircularProgress(
                    modifier = modifier
                        .align(CenterHorizontally),
                    isDisplayed = loading
                )
            }
        }
        AnimatedVisibility(visible = empty) {
            Column(
                modifier = modifier
                    .fillMaxSize(),
                horizontalAlignment = CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Image(
                    modifier = modifier
                        .size(290.dp),
                    painter = painterResource(R.drawable.listimmage_1),
                    contentDescription = "Login Backgroud",
                    alignment = Center
                )
                Spacer(modifier = Modifier.size(120.dp))
            }
        }
        AnimatedVisibility(visible = !empty) {
            Column {
                LazyColumn() {
                    items(
                        items = friendList.sortedByDescending { it.fullname },
                    ) { user ->
                        locationFriendsMap[user.mail.replace(".", "-")]?.let {
                            ExpandableCard(
                                modifier = modifier,
                                owner = user,
                                location = it,
                                navController = navController,
                                onDeleteClick = { usersViewModel.deleteFriend(user.mail) })
                        }
                    }
                }
                if (friendList.size == 2) {
                    DummyCard(modifier = modifier)
                }
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ExpandableCard(
    modifier: Modifier,
    owner: User,
    onDeleteClick: () -> Unit,
    navController: NavHostController,
    titleFontSize: TextUnit = MaterialTheme.typography.titleLarge.fontSize,
    padding: Dp = 8.dp,
    location: Friend,
) {
    var expandedState by remember { mutableStateOf(false) }
    val rotationState by animateFloatAsState(
        targetValue = if (expandedState) 180f else 0f
    )

    Surface(
        modifier = Modifier
            .padding(3.dp)
            .animateContentSize(
                animationSpec = tween(
                    durationMillis = 500,
                    easing = LinearOutSlowInEasing
                )
            ),
        color = MaterialTheme.colorScheme.surfaceVariant.copy(alpha = 0.5f),
        contentColor = MaterialTheme.colorScheme.onSurface,
        shape = MaterialTheme.shapes.large
    ) {
        ElevatedCard(
            enabled = true,
            modifier = modifier
                .wrapContentSize()
                .padding(5.dp)
                .shadow(
                    elevation = 3.dp,
                    shape = MaterialTheme.shapes.large,
                ),
            elevation = CardDefaults.cardElevation(),
            colors = CardDefaults.elevatedCardColors(
                containerColor = MaterialTheme.colorScheme.surfaceVariant,
                contentColor = MaterialTheme.colorScheme.onSurface,
            ),
            shape = MaterialTheme.shapes.large,
            onClick = {
                expandedState = !expandedState
            }
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(padding)
            ) {
                Row(
                    verticalAlignment = CenterVertically
                ) {
                    Surface(
                        color = MaterialTheme.colorScheme.surface.copy(alpha = 0.5f),
                        contentColor = MaterialTheme.colorScheme.onSurface,
                        tonalElevation = 5.dp,
                        shape = MaterialTheme.shapes.extraLarge
                    ) {
                        Box(
                            modifier = modifier
                                .size(40.dp)
                                .align(Alignment.Bottom),
                            contentAlignment = Center
                        )
                        {
                            Text(
                                textAlign = TextAlign.Justify,
                                fontSize = MaterialTheme.typography.titleLarge.fontSize,
                                fontWeight = MaterialTheme.typography.titleLarge.fontWeight,
                                fontFamily = MaterialTheme.typography.titleLarge.fontFamily,
                                text = owner.fullname[0].uppercase()
                            )
                        }
                    }
                    Spacer(modifier = Modifier.size(10.dp))
                    Column(
                        horizontalAlignment = Start,
                        modifier = Modifier
                            .weight(3f)
                    ) {
                        Text(
                            text = owner.fullname,
                            fontSize = titleFontSize,
                            fontWeight = FontWeight.Bold,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        )
                    }
                    Row(
                        horizontalArrangement = Arrangement.End,
                        modifier = Modifier
                            .weight(1f)
                    ) {
                        IconButton(
                            modifier = Modifier
                                .weight(1f)
                                .alpha(ContentAlpha.medium)
                                .rotate(rotationState),
                            onClick = {
                                expandedState = !expandedState
                            }
                        ) {
                            Icon(
                                imageVector = Icons.Default.ArrowDropDown,
                                contentDescription = "Drop-Down Arrow",
                                tint = MaterialTheme.colorScheme.onPrimaryContainer
                            )
                        }
                    }
                }
                if (expandedState) {
                    androidx.compose.material.Divider(
                        modifier = modifier
                            .padding(5.dp),
                        thickness = 1.dp,
                        color = Color.Gray
                    )
                    val formattedNumber = owner.mobile.chunked(3).joinToString(" ")
                    Column {
                        TextOutput(
                            owner = owner,
                            ownerSpec = if (owner.mobile == "e") {
                                ""
                            } else {
                                formattedNumber
                            },
                            label = stringResource(R.string.card_text_number),
                            description = stringResource(R.string.card_text_number_result))
                        TextOutput(owner = owner,
                            ownerSpec = if (owner.mail == "") {
                                ""
                            } else {
                                owner.mail
                            },
                            label = stringResource(R.string.card_text_mail),
                            description = stringResource(R.string.card_text_mail_result))

                        Spacer(modifier = Modifier.size(10.dp))
                        Divider(thickness = 0.5.dp, color = Color.Gray)
                        Spacer(modifier = Modifier.size(10.dp))

                        TextOutputLink(
                            owner = owner,
                            ownerSpec =
                            if (owner.settings["facebook"] == true) {
                                if (owner.facebook == "e") {
                                    ""
                                } else {
                                    owner.facebook
                                }
                            } else {
                                stringResource(R.string.list_hidden)
                            },
                            label = stringResource(R.string.card_text_facebook),
                            description = stringResource(R.string.card_text_facebook_result),
                            uriLink =
                            if (owner.facebook == "e") {
                                "https://www.facebook.com"
                            } else {
                                "https://www.facebook.com/" + owner.facebook
                            },
                        )
                        TextOutputLink(owner = owner,
                            ownerSpec =
                            if (owner.settings["instagram"] == true) {
                                if (owner.instagram == "e") {
                                    ""
                                } else {
                                    owner.instagram
                                }
                            } else {
                                stringResource(R.string.list_hidden)
                            },
                            label = stringResource(R.string.card_text_instagram),
                            description = stringResource(R.string.card_text_instagram_result),
                            uriLink =
                            if (owner.instagram == "e") {
                                "https://www.instagram.com"
                            } else {
                                "https://www.instagram.com/" + owner.instagram
                            }
                        )
                        TextOutputLink(owner = owner,
                            ownerSpec =
                            if (owner.settings["twitter"] == true) {
                                if (owner.twitter == "e") {
                                    ""
                                } else {
                                    owner.twitter
                                }
                            } else {
                                stringResource(R.string.list_hidden)
                            },
                            label = stringResource(R.string.card_text_twitter),
                            description = stringResource(R.string.card_text_twitter_result),
                            uriLink =
                            if (owner.twitter == "e") {
                                "https://twitter.com"
                            } else {
                                "https://twitter.com/" + owner.instagram
                            }
                        )
                        androidx.compose.material.Divider(
                            modifier = modifier
                                .padding(top = 10.dp),
                            thickness = 0.5.dp,
                            color = Color.Gray
                        )
                        Row(
                            modifier = Modifier
                                .fillMaxSize()
                                .align(CenterHorizontally),
                            horizontalArrangement = Arrangement.Center,
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            CustomButton(
                                modifier = Modifier
                                    .weight(1f),
                                onClick = {
                                    navController.navigate("Map?lat=${location.lat}&lot=${location.long}") {
                                        popUpTo(navController.graph.findStartDestination().id) {
                                            saveState = true
                                        }
                                        launchSingleTop = true
                                    }
                                },
                                status = location.lat != "e" && location.long != "e",
                                text = stringResource(R.string.card_button_map),
                                topPadding = 25
                            )
                            CustomButton(
                                modifier = Modifier
                                    .weight(1f),
                                onClick = { onDeleteClick() },
                                text = stringResource(R.string.card_button_delete),
                                topPadding = 25
                            )
                        }

                    }
                }
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DummyCard(
    modifier: Modifier,
    titleFontSize: TextUnit = MaterialTheme.typography.titleLarge.fontSize,
    padding: Dp = 12.dp,
) {
    var expandedState by remember { mutableStateOf(false) }
    val rotationState by animateFloatAsState(
        targetValue = if (expandedState) 180f else 0f
    )

    ElevatedCard(
        enabled = true,
        modifier = modifier
            .fillMaxWidth()
            .animateContentSize(
                animationSpec = tween(
                    durationMillis = 300,
                    easing = LinearOutSlowInEasing
                )
            )
            .padding(5.dp)
            .shadow(
                elevation = 3.dp,
                shape = MaterialTheme.shapes.large,
            ),
        elevation = CardDefaults.cardElevation(),
        colors = CardDefaults.elevatedCardColors(
            containerColor = MaterialTheme.colorScheme.surfaceVariant,
            contentColor = MaterialTheme.colorScheme.onSurface,
        ),
        shape = MaterialTheme.shapes.large,
        onClick = {
            expandedState = !expandedState
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(padding)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    horizontalAlignment = Start,
                    modifier = Modifier
                        .weight(3f)
                ) {
                    Text(
                        text = stringResource(R.string.dummy_hed),
                        fontSize = titleFontSize,
                        fontWeight = FontWeight.Bold,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                }
                Row(
                    horizontalArrangement = Arrangement.End,
                    modifier = Modifier
                        .weight(1f)
                ) {
                    IconButton(
                        modifier = Modifier
                            .weight(1f)
                            .alpha(ContentAlpha.medium)
                            .rotate(rotationState),
                        onClick = {
                            expandedState = !expandedState
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Default.ArrowDropDown,
                            contentDescription = "Drop-Down Arrow",
                            tint = MaterialTheme.colorScheme.onPrimaryContainer
                        )
                    }
                }
            }
            if (expandedState) {
                Spacer(modifier = Modifier.size(20.dp))
                Column {
                    DummyText(
                        label = stringResource(R.string.dummy_tip1),
                        description = stringResource(R.string.dummy_tip1_desc))
                    Spacer(modifier = Modifier.size(8.dp))
                    DummyText(
                        label = stringResource(R.string.dummy_tip3),
                        description = stringResource(R.string.dummy_tip3_desc))
                    Spacer(modifier = Modifier.size(8.dp))
                    DummyText(
                        label = stringResource(R.string.dummy_tip2),
                        description = stringResource(R.string.dummy_tip2_desc))
                    Spacer(modifier = Modifier.size(8.dp))
                    Text(
                        text = stringResource(R.string.dummy_info),
                        fontStyle = MaterialTheme.typography.labelSmall.fontStyle,
                        fontFamily = MaterialTheme.typography.labelSmall.fontFamily,
                        fontWeight = MaterialTheme.typography.labelSmall.fontWeight,
                        fontSize = MaterialTheme.typography.labelSmall.fontSize,
                    )
                    Spacer(modifier = Modifier.size(20.dp))
                    //Divider(thickness = 0.5.dp, color = Color.Gray)
                }
            }
        }
    }
}

@Composable
fun DummyText(
    label: String,
    description: String,
    descriptionMaxLines: Int = 4,
) {
    Column(
        modifier = Modifier
            .padding(bottom = 1.dp, top = 1.dp)
    ) {

        Text(
            text = label,
            fontSize = MaterialTheme.typography.labelLarge.fontSize,
            fontWeight = MaterialTheme.typography.labelLarge.fontWeight,
            fontFamily = MaterialTheme.typography.labelLarge.fontFamily
        )
        Text(
            text = description,
            fontSize = MaterialTheme.typography.bodySmall.fontSize,
            fontWeight = MaterialTheme.typography.bodySmall.fontWeight,
            fontFamily = MaterialTheme.typography.bodySmall.fontFamily,
            fontStyle = MaterialTheme.typography.bodySmall.fontStyle,
            maxLines = descriptionMaxLines,
            overflow = TextOverflow.Ellipsis
        )
    }
}