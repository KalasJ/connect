package com.mushhead.connect.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.mushhead.connect.R

/**
 * 06.03.2023 CREATE
 * Vytvořeno pro znovu použítí v jiných screenech.
 */
@Composable
fun CustomButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    status: Boolean = true,
    text: String = stringResource(id = R.string.Profile_edit_button),
    topPadding: Int = 10,
    bottomPadding: Int = 20,
) {
    Column(
        modifier = modifier
            .padding(top = topPadding.dp, bottom = bottomPadding.dp)
            .height(40.dp)
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        ElevatedButton(
            modifier = modifier,

            elevation = ButtonDefaults.elevatedButtonElevation(
                defaultElevation = 8.dp
            ),
            shape = androidx.compose.material3.MaterialTheme.shapes.extraLarge,
            colors = ButtonDefaults.buttonColors(),
            enabled = status,
            onClick = { onClick() }
        )
        {
            Text(
                fontSize = androidx.compose.material3.MaterialTheme.typography.labelLarge.fontSize,
                text = text,
            )
        }
    }
}