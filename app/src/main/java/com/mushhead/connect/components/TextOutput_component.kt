package com.mushhead.connect.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp


@Composable
fun TextOutput(
    owner: com.mushhead.connect.utils.User,
    ownerSpec: String,
    label: String,
    description: String,
    descriptionMaxLines: Int = 1,
) {
    val clipboardManager: androidx.compose.ui.platform.ClipboardManager =
        LocalClipboardManager.current

    Column(
        modifier = Modifier
            .padding(bottom = 1.dp, top = 1.dp)
    ) {

        Text(
            fontSize = MaterialTheme.typography.labelLarge.fontSize,
            fontWeight = MaterialTheme.typography.labelLarge.fontWeight,
            fontFamily = MaterialTheme.typography.labelLarge.fontFamily,
            text = label,
        )
        ClickableText(
            text =
            if (ownerSpec == "") {
                AnnotatedString(owner.fullname + " " + description)
            } else {
                AnnotatedString(ownerSpec)
            },
            onClick = {
                if (ownerSpec != "") {
                    clipboardManager.setText(AnnotatedString(ownerSpec))
                }
            },
            style = TextStyle(
                fontSize = MaterialTheme.typography.bodySmall.fontSize,
                fontWeight = MaterialTheme.typography.bodySmall.fontWeight,
                fontFamily = MaterialTheme.typography.bodySmall.fontFamily,
                fontStyle = MaterialTheme.typography.bodySmall.fontStyle,
                color = MaterialTheme.colorScheme.onSurface
            ),
            maxLines = descriptionMaxLines,
            overflow = TextOverflow.Ellipsis
        )
    }
}