package com.mushhead.connect.views

import android.app.Activity
import android.content.ContentValues
import android.util.Log
import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.mushhead.connect.R
import com.mushhead.connect.components.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

@Composable
fun LoginScreen(
    modifier: Modifier = Modifier,
    auth: FirebaseAuth,
    navController: NavController,
) {
    var mail by remember { mutableStateOf("") }
    var fullname by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var login by remember { mutableStateOf(false) }
    var register by remember { mutableStateOf(false) }
    val context = LocalContext.current as Activity

    StatlessLoginScreen(
        modifier = modifier,
        auth = auth,
        navController = navController,
        context = context,
        login = login,
        register = register,
        mail = mail,
        fullname = fullname,
        password = password,
        onLogin = {login = it},
        onRegister = {register = it},
        onMail = { mail = it },
        onFullname = { fullname = it },
        onPassword = { password = it },
    )
}

@Composable
fun StatlessLoginScreen(
    modifier: Modifier = Modifier,
    auth: FirebaseAuth,
    navController: NavController,
    mail: String,
    fullname: String,
    password: String,
    context: Activity,
    onMail: (String) -> Unit,
    onFullname: (String) -> Unit,
    onPassword: (String) -> Unit,
    login: Boolean,
    register: Boolean,
    onRegister: (Boolean) -> Unit,
    onLogin: (Boolean) -> Unit,
) {

    Column(
        modifier = modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Body(
            modifier = modifier,
            auth = auth,
            navController = navController,
            mail = mail,
            fullname = fullname,
            password = password,
            context = context,
            login = login,
            register = register,
            onMail = onMail,
            onFullname = onFullname,
            onPassword = onPassword,
            onLogin = onLogin,
            onRegister = onRegister,
        )
    }
}

@Composable
fun Body(
    modifier: Modifier = Modifier,
    auth: FirebaseAuth,
    navController: NavController,
    mail: String,
    fullname: String,
    password: String,
    context: Activity,
    onMail: (String) -> Unit,
    onFullname: (String) -> Unit,
    onPassword: (String) -> Unit,
    login: Boolean,
    register: Boolean,
    onLogin: (Boolean) -> Unit,
    onRegister: (Boolean) -> Unit,
) {
    Column(
        modifier = modifier
            .wrapContentSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        AnimatedVisibility(visible = !login && !register) {
            UpperPart(
                modifier = modifier
            )
        }
        LowerPart(
            login = login,
            register = register,
            onLogin = onLogin,
            onRegister = onRegister
        )
        AnimatedVisibility(visible = login) {
            LoginPart(
                auth = auth,
                navController = navController,
                mail = mail,
                password = password,
                context = context,
                onMail = onMail,
                onPassword = onPassword
            )
        }
        AnimatedVisibility(visible = register) {
            RegisterPart(
                auth = auth,
                navController = navController,
                mail = mail,
                fullname = fullname,
                password = password,
                context = context,
                onMail = onMail,
                onFullname = onFullname,
                onPassword = onPassword)
        }
    }
}

@Composable
fun UpperPart(
    modifier: Modifier
){
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        Image(
            modifier = modifier
                .weight(1f, false)
                .size(250.dp),
            painter = painterResource(id = R.drawable.login_no_background),
            contentDescription = "Login Backgroud"
        )
        Text(
            modifier = modifier
                .padding(5.dp),
            fontStyle = MaterialTheme.typography.titleLarge.fontStyle,
            fontWeight = MaterialTheme.typography.titleLarge.fontWeight,
            fontSize = MaterialTheme.typography.titleLarge.fontSize,
            fontFamily = MaterialTheme.typography.titleLarge.fontFamily,
            text = stringResource(R.string.log_label)
        )
    }
}

@Composable
fun LowerPart(
    modifier: Modifier = Modifier,
    login: Boolean,
    register: Boolean,
    onLogin: (Boolean) -> Unit,
    onRegister: (Boolean) -> Unit
){
    Text(
        modifier = modifier
            .padding(1.dp),
        textAlign = TextAlign.Center,
        fontStyle = MaterialTheme.typography.bodySmall.fontStyle,
        fontWeight = MaterialTheme.typography.bodySmall.fontWeight,
        fontSize = MaterialTheme.typography.bodySmall.fontSize,
        fontFamily = MaterialTheme.typography.bodySmall.fontFamily,
        text = stringResource(R.string.log_desc)
    )
    Row(
        modifier = modifier
            .fillMaxWidth()
            .width(100.dp),
    ) {
        CustomButton(
            modifier = modifier
                .weight(1f),
            onClick = {
                onLogin(!login)
                if (register) {
                    onRegister(false)
                }
            },
            text = stringResource(R.string.reg_log_button))
        CustomButton(
            modifier = modifier
                .weight(1f),
            onClick = {
                onRegister(!register)
                if (login) {
                    onLogin(false)
                }
            },
            text = stringResource(R.string.reg_reg_button))
    }
}

@Composable
fun LoginPart(
    modifier: Modifier = Modifier,
    auth: FirebaseAuth,
    navController: NavController,
    mail: String,
    password: String,
    context: Activity,
    onMail: (String) -> Unit,
    onPassword: (String) -> Unit,
) {
    Column(
        modifier = modifier
            .width(350.dp)
    ) {
        InputText(modifier = modifier,
            value = mail,
            onValue = onMail,
            label = stringResource(R.string.Profile_Mail_label),
            placeHolder = stringResource(R.string.Profile_Mail_spaceHolder),
            supportingNull = stringResource(id = R.string.edit_input_mail_sup)
        )
        InputPasswordLog(modifier = modifier,
            value = password,
            onValue = onPassword,
            label = stringResource(R.string.Profile_password_spaceHolder),
            placeHolder = stringResource(R.string.Profile_password_label),
            supportingNull = stringResource(id = R.string.edit_input_passw_sup)
        )
        CustomButton(
            status = !(mail == "" || password == ""),
            onClick = {
                auth.signInWithEmailAndPassword(mail, password)
                    .addOnCompleteListener(context) { task ->
                        if (task.isSuccessful) {
                            navController.popBackStack()
                            navController.navigate(com.mushhead.connect.utils.MainScreen.route)
                        } else {
                            Log.w(ContentValues.TAG,
                                "signInWithEmail:failure",
                                task.exception)
                            Toast.makeText(context, R.string.auth_log_error,
                                Toast.LENGTH_SHORT).show()
                        }
                    }
            },
            text = stringResource(R.string.reg_button_Create)
        )
    }
}

@Composable
fun RegisterPart(
    modifier: Modifier = Modifier,
    auth: FirebaseAuth,
    navController: NavController,
    mail: String,
    fullname: String,
    password: String,
    context: Activity,
    onMail: (String) -> Unit,
    onFullname: (String) -> Unit,
    onPassword: (String) -> Unit,
) {
    Column(
        modifier = modifier
            .width(350.dp)
    ) {
        InputText(modifier = modifier,
            value = mail,
            onValue = onMail,
            label = stringResource(R.string.Profile_Mail_label),
            placeHolder = stringResource(R.string.Profile_Mail_spaceHolder),
            supportingNull = stringResource(id = R.string.edit_input_mail_sup)
        )
        InputText(modifier = modifier,
            value = fullname,
            onValue = onFullname,
            label = stringResource(R.string.Profile_realName_label),
            placeHolder = stringResource(R.string.Profile_realName_spaceHolder),
            supportingNull = stringResource(id = R.string.edit_input_fullname_sup)
        )
        InputPasswordReg(modifier = modifier,
            value = password,
            onValue = onPassword,
            label = stringResource(R.string.Profile_password_spaceHolder),
            placeHolder = stringResource(R.string.Profile_password_label),
            supportingNull = stringResource(id = R.string.edit_input_passw_sup),
            supportingToSmall = stringResource(R.string.login_password_tosmall)
        )
        CustomButton(
            status = !(mail == "" || password == "" || fullname == "" || password.length < 3),
            onClick = {
                auth.createUserWithEmailAndPassword(mail, password)
                    .addOnCompleteListener(context) { task ->
                        if (task.isSuccessful) {
                            val db = Firebase.firestore
                            val user = hashMapOf(
                                "fullname" to fullname,
                                "mail" to mail,
                                "mobile" to "",
                                "facebook" to "",
                                "instagram" to "",
                                "twitter" to "",
                                "friends" to mapOf<String, String>(),
                                "settings" to mapOf(
                                    "facebook" to true,
                                    "instagram" to true,
                                    "twitter" to true,
                                )
                            )

                            db.collection("users").document(mail).set(user)
                                .addOnSuccessListener {
                                    Log.d(ContentValues.TAG,
                                        "Succes")
                                }.addOnFailureListener { e ->
                                    Log.w(ContentValues.TAG, "Error adding document", e)
                                }
                            navController.popBackStack()
                            navController.navigate(com.mushhead.connect.utils.MainScreen.route)
                        } else {
                            Log.w(ContentValues.TAG,
                                "createUserWithEmail:failure",
                                task.exception)
                            Toast.makeText(context, R.string.auth_reg_error,
                                Toast.LENGTH_SHORT).show()
                        }
                    }
            },
            text = stringResource(R.string.reg_button_Create)
        )
    }
}