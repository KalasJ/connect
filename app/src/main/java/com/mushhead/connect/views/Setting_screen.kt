package com.mushhead.connect.views

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.mushhead.connect.R
import com.mushhead.connect.components.SwichRow
import com.mushhead.connect.viewmodels.UsersViewModel

/**
 * 27.02.2023 UPDATE
 * Nový vzhled, příprava na přidání nové tabulky do databaze s uživatelskými předvolbami.
 * 28.02.2023 UPDATE
 * Upraven vzhled, zprovozněny všechny funčknosti + nová tabulka v databázi.
 * 28.03.2023 UPDATE
 * HOTOVO
 */

@Composable
fun SettingScreen(
    modifier: Modifier = Modifier,
    usersViewModel: UsersViewModel = hiltViewModel(),
) {
    StatlessSettingScreen(
        modifier = modifier,
        usersViewModel = usersViewModel
    )

}

@Composable
fun StatlessSettingScreen(
    modifier: Modifier,
    usersViewModel: UsersViewModel
){
    val setting = usersViewModel.owner.value.settings
    var facebook by remember { mutableStateOf(true) }
    var instagram by mutableStateOf(true)
    var twitter by mutableStateOf(true)


    if (setting.isNotEmpty()) {
        facebook = setting["facebook"] == true
        instagram = setting["instagram"] == true
        twitter = setting["twitter"] == true
    }


    Column(
        modifier = modifier
            .padding(10.dp)
            .fillMaxHeight()
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Spacer(modifier = modifier.size(10.dp))
        Row(
            modifier = modifier
                .align(Alignment.Start)
                .weight(1f)
        ) {
            Column(
                modifier = modifier
                    .weight(1f)
                    .fillMaxSize()
            ) {
                Text(
                    modifier = modifier
                        .padding(bottom = 5.dp),
                    fontSize = MaterialTheme.typography.titleMedium.fontSize,
                    fontWeight = MaterialTheme.typography.titleMedium.fontWeight,
                    fontFamily = MaterialTheme.typography.titleMedium.fontFamily,
                    text = stringResource(id = R.string.Settings_hed1))
                SwichRow(
                    modifier = modifier
                        .padding(start = 5.dp),
                    lambda = {
                        facebook = !facebook
                        usersViewModel.updateSettings("facebook", facebook)
                    },
                    checked = facebook,
                    R.string.Settings_facebook,
                    R.string.Settings_facebook_des)
                SwichRow(modifier = modifier
                    .padding(start = 5.dp),
                    lambda = {
                        instagram = !instagram
                        usersViewModel.updateSettings("instagram", instagram)
                    },
                    checked = instagram,
                    R.string.Settings_instagram,
                    R.string.Settings_instagram_des)
                SwichRow(modifier = modifier
                    .padding(start = 5.dp),
                    lambda = {
                        twitter = !twitter
                        usersViewModel.updateSettings("twitter", twitter)
                    },
                    checked = twitter,
                    R.string.Settings_twitter,
                    R.string.Settings_twitter_des)
            }
        }
        Column(
            modifier = modifier
                .weight(1f)
                .fillMaxSize(),
            horizontalAlignment = Alignment.End,
            verticalArrangement = Arrangement.Bottom
        ) {
            Text(
                modifier = modifier,
                text = stringResource(id = R.string.Settings_app_version))
        }
    }
}