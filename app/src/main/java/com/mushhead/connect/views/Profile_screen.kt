package com.mushhead.connect.views

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.mushhead.connect.R
import com.mushhead.connect.components.CustomButton
import com.mushhead.connect.components.CustomText
import com.mushhead.connect.utils.User
import com.mushhead.connect.viewmodels.ProfileViewModel
import com.mushhead.connect.viewmodels.UsersViewModel


/**
 * 09.02.2023 CREATE
 * Implementoval jsem základy profile screeny
 * 12.02.2023 UPDATE
 * Upravil jsem kod aby byl přehlednější
 * 13.02.2023 UPDATE
 * Problém, chtěl jsem implementovat první API od Facebooku pro získávání dat o účtech. Zjistil jse že není oficiální návod a po 4h snahy implmentovat co jse našel jsem selhal.
 * Prozatím jsem zvolil lehčí řešení kde uživatel sám vložíl link na svůj FB, INS, Twitter ať dosáhnu alespoň nějakého postupu za dnešek a vrátím se k tomu v budoucnu.
 * 28.02.2023 UPDATE
 * Velký refaktoring kodu a přidání pár zabezpečení.
 * 06.03.2023 UPDATE
 * Vytvořil jsem úplněn nový vzhled a rozdělil kod do dvou screen, tak aby to nějak vypadalo.
 * 08.03.2023 UPDATE
 * Usoudil jsem že profilové obrázky tam stejně nemají žadný význam, jde jen o předání infa.
 * 28.03.2023  UPDATE
 * HOTOVO
 */
@Composable
fun ProfileScreen(
    modifier: Modifier = Modifier,
    onClickEdit: () -> Unit,
    usersViewModel: UsersViewModel = hiltViewModel(),
) {
    val owner = usersViewModel.owner.value

    StatlessProfileScreen(
        modifier = modifier,
        onClickEdit = onClickEdit,
        owner = owner)
}

@Composable
fun StatlessProfileScreen(
    modifier: Modifier,
    profileViewModel: ProfileViewModel = hiltViewModel(),
    onClickEdit: () -> Unit,
    owner: User,
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .paint(
                painter = painterResource(id = R.drawable.profile_1),
                contentScale = ContentScale.Crop
            )
    ) {
        Spacer(modifier = modifier.size(30.dp))
        Column(
            modifier = modifier
                .fillMaxSize()
                .padding(top = 20.dp)
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {
            Row() {
                Spacer(modifier = modifier.weight(1f))
                Surface(
                    modifier = modifier
                        .weight(20f),
                    color = MaterialTheme.colorScheme.surfaceVariant.copy(alpha = (0.97f)),
                    contentColor = MaterialTheme.colorScheme.onSurface,
                    shadowElevation = 5.dp,
                    shape = MaterialTheme.shapes.large,
                    border = BorderStroke(0.5.dp, MaterialTheme.colorScheme.onSurface)
                ) {
                    Column(
                        modifier = modifier
                            .padding(10.dp)
                    ) {
                        BodyText(
                            modifier = modifier,
                            profileViewModel,
                            owner = owner)
                        CustomButton(
                            onClick = onClickEdit,
                            topPadding = 30
                        )
                    }
                }
                Spacer(modifier = modifier.weight(1f))
            }
        }
    }

}

@Composable
fun BodyText(
    modifier: Modifier,
    profileViewModel: ProfileViewModel,
    owner: User,
) {
    Text(
        modifier = modifier
            .padding(bottom = 10.dp, top = 20.dp),
        text = stringResource(R.string.profile_text_label),
        fontStyle = MaterialTheme.typography.titleMedium.fontStyle,
        fontWeight = MaterialTheme.typography.titleMedium.fontWeight,
        fontSize = MaterialTheme.typography.titleMedium.fontSize
    )

    CustomText(icon = Icons.Outlined.PersonOutline,
        headline = stringResource(R.string.profile_text_custom_name),
        text = owner.fullname
    )

    val formattedNumber =  owner.mobile.chunked(3).joinToString(" ")
    CustomText(icon = Icons.Outlined.MobileScreenShare,
        headline = stringResource(R.string.profile_text_custom_mobile),
        text = if (profileViewModel.emptyCheck(owner.mobile)) {
            formattedNumber
        } else {
            stringResource(R.string.profile_text_custom_mobile_cont)
        })

    CustomText(icon = Icons.Outlined.Mail,
        headline = stringResource(R.string.profile_text_custom_mail),
        text =
        if (profileViewModel.emptyCheck(owner.mail)) {
            owner.mail
        } else {
            stringResource(R.string.profile_text_custom_mail_cont)
        })

    Spacer(modifier = modifier.size(30.dp))

    CustomText(icon = Icons.Outlined.Link,
        headline = stringResource(R.string.profile_text_custom_facebook),
        text =
        if (profileViewModel.emptyCheck(owner.facebook)) {
            owner.facebook
        } else {
            stringResource(R.string.profile_text_custom_facebook_cont)
        })
    CustomText(icon = Icons.Outlined.Link,
        headline = stringResource(R.string.profile_text_custom_instagram),
        text =
        if (profileViewModel.emptyCheck(owner.instagram)) {
            owner.instagram
        } else {
            stringResource(R.string.profile_text_custom_instagram_cont)
        })
    CustomText(icon = Icons.Outlined.Link,
        headline = stringResource(R.string.profile_text_custom_twitter),
        text =
        if (profileViewModel.emptyCheck(owner.twitter)) {
            owner.twitter
        } else {
            stringResource(R.string.profile_text_custom_twitter_cont)
        })
}