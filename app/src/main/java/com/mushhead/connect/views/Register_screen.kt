package com.mushhead.connect.views

import android.app.Activity
import android.content.ContentValues
import android.content.ContentValues.TAG
import android.util.Log
import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.mushhead.connect.R
import com.mushhead.connect.components.*
import com.mushhead.connect.utils.MainScreen
import com.mushhead.connect.utils.OnBoardingPage
import com.mushhead.connect.viewmodels.WellcomeViewModel
import com.facebook.*
import com.google.accompanist.pager.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


/**
 * 27.02.2023 CREATED
 * Musel jsem vymyslet způsob pro vytvoření vlastníka aplikace
 * 28.03.2023 UPDATE
 * HOTOVO
 */
@OptIn(ExperimentalPagerApi::class)
@Composable
fun RegisterScreen(
    auth: FirebaseAuth,
    navHostController: NavHostController,
    wellcomeViewModel: WellcomeViewModel = hiltViewModel(),
) {
    val pageState = rememberPagerState()
    var mail by remember { mutableStateOf("") }
    var fullname by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var login by remember { mutableStateOf(false) }
    var register by remember { mutableStateOf(false) }
    val context = LocalContext.current as Activity

    val pages = listOf(
        OnBoardingPage.First,
        OnBoardingPage.Second,
        OnBoardingPage.Third
    )

    StatlessRegisterScreen(
        auth = auth,
        pageState = pageState,
        context = context,
        navHostController = navHostController,
        wellcomeViewModel = wellcomeViewModel,
        pages = pages,
        login = login,
        register = register,
        mail = mail,
        password = password,
        fullname = fullname,
        onLogin = {login = it},
        onRegister = {register = it},
        onMail = { mail = it },
        onPassword = { password = it },
        onFullname = { fullname = it }
    )
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun StatlessRegisterScreen(
    modifier: Modifier = Modifier,
    auth: FirebaseAuth,
    context: Activity,
    pageState: PagerState,
    navHostController: NavHostController,
    wellcomeViewModel: WellcomeViewModel,
    pages: List<OnBoardingPage>,
    login: Boolean,
    register: Boolean,
    mail: String,
    fullname: String,
    password: String,
    onLogin: (Boolean) -> Unit,
    onRegister: (Boolean) -> Unit,
    onMail: (String) -> Unit,
    onFullname: (String) -> Unit,
    onPassword: (String) -> Unit,
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Spacer(modifier = modifier.size(50.dp))
        HorizontalPager(
            modifier = modifier
                .size(450.dp),
            count = 3,
            state = pageState,
            verticalAlignment = Alignment.Top
        ) { position ->
            Body(
                modifier = modifier,
                auth = auth,
                context = context,
                onBoardingPage = pages[position],
                pagerState = pageState,
                navHostController = navHostController,
                wellcomeViewModel = wellcomeViewModel,
                login = login,
                register = register,
                mail = mail,
                fullname = fullname,
                password = password,
                onLogin = onLogin,
                onRegister = onRegister,
                onFullname = onFullname,
                onMail = onMail,
                onPassword = onPassword
            )
        }
        Spacer(modifier = modifier.size(100.dp))
        HorizontalPagerIndicator(
            modifier = modifier
                .wrapContentSize()
                .align(Alignment.CenterHorizontally),
            pagerState = pageState,
            activeColor = MaterialTheme.colorScheme.primary
        )
    }
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun Body(
    modifier: Modifier,
    auth: FirebaseAuth,
    context: Activity,
    pagerState: PagerState,
    navHostController: NavHostController,
    wellcomeViewModel: WellcomeViewModel,
    login: Boolean,
    register: Boolean,
    password: String,
    mail: String,
    fullname: String,
    onLogin: (Boolean) -> Unit,
    onRegister: (Boolean) -> Unit,
    onPassword: (String) -> Unit,
    onMail: (String) -> Unit,
    onFullname: (String) -> Unit,
    onBoardingPage: OnBoardingPage,
) {
    Column(
        modifier = modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        AnimatedVisibility(visible = !login && !register) {
            LoginRegisterText(
                modifier = modifier,
                onBoardingPage = onBoardingPage
            )
        }
        AnimatedVisibility( visible = pagerState.currentPage != 2)
        {
            Text(
                modifier = modifier
                    .padding(5.dp),
                textAlign = TextAlign.Center,
                fontStyle = MaterialTheme.typography.bodySmall.fontStyle,
                fontWeight = MaterialTheme.typography.bodySmall.fontWeight,
                fontSize = MaterialTheme.typography.bodySmall.fontSize,
                fontFamily = MaterialTheme.typography.bodySmall.fontFamily,
                text = stringResource(onBoardingPage.description)
            )
        }
        AnimatedVisibility( visible = pagerState.currentPage == 2)
        {
            Column(
                modifier = modifier
                    .wrapContentSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                LoginRegisterButtons(
                    modifier = modifier,
                    onBoardingPage = onBoardingPage,
                    login = login,
                    register = register,
                    onLogin = onLogin,
                    onRegister = onRegister
                )
                AnimatedVisibility(visible = login) {
                  LoginPartReg(
                      auth = auth,
                      wellcomeViewModel = wellcomeViewModel,
                      navHostController = navHostController,
                      mail = mail,
                      password = password,
                      context = context,
                      onMail = onMail,
                      onPassword = onPassword
                  )
                }
                AnimatedVisibility(visible = register) {
                    RegisterPartReg(
                        modifier = modifier,
                        auth = auth,
                        context = context,
                        navHostController = navHostController,
                        wellcomeViewModel = wellcomeViewModel,
                        password = password,
                        onPassword = onPassword,
                        mail = mail,
                        onMail = onMail,
                        fullname = fullname,
                        onFullname = onFullname)
                }
            }
        }
    }
}

@Composable
fun LoginRegisterButtons(
    modifier: Modifier,
    onBoardingPage: OnBoardingPage,
    login: Boolean,
    register: Boolean,
    onLogin: (Boolean) -> Unit,
    onRegister: (Boolean) -> Unit,
){
    Text(
        modifier = modifier
            .padding(5.dp),
        textAlign = TextAlign.Center,
        fontStyle = MaterialTheme.typography.bodySmall.fontStyle,
        fontWeight = MaterialTheme.typography.bodySmall.fontWeight,
        fontSize = MaterialTheme.typography.bodySmall.fontSize,
        fontFamily = MaterialTheme.typography.bodySmall.fontFamily,
        text = stringResource(onBoardingPage.description)
    )
    Row(
        modifier = modifier
            .fillMaxWidth()
            .width(100.dp),
    ) {
        CustomButton(
            modifier = modifier
                .weight(1f),
            onClick = {
                onLogin(!login)
                if (register) {
                    onRegister(false)
                }
            },
            text = stringResource(R.string.reg_log_button))
        CustomButton(
            modifier = modifier
                .weight(1f),
            onClick = {
                onRegister(!register)
                if (login) {
                    onLogin(false)
                }
            },
            text = stringResource(R.string.reg_reg_button))
    }
}

@Composable
fun LoginRegisterText(
    modifier: Modifier,
    onBoardingPage: OnBoardingPage
){
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        Image(
            modifier = modifier
                .weight(1f, false)
                .size(290.dp),
            painter = painterResource(onBoardingPage.image),
            contentDescription = "Login Backgroud"
        )
        Text(
            modifier = modifier
                .padding(5.dp),
            fontStyle = MaterialTheme.typography.titleLarge.fontStyle,
            fontWeight = MaterialTheme.typography.titleLarge.fontWeight,
            fontSize = MaterialTheme.typography.titleLarge.fontSize,
            fontFamily = MaterialTheme.typography.titleLarge.fontFamily,
            text = stringResource(onBoardingPage.title)
        )
    }
}

@Composable
fun LoginPartReg(
    modifier: Modifier = Modifier,
    auth: FirebaseAuth,
    context: Activity,
    wellcomeViewModel: WellcomeViewModel,
    navHostController: NavHostController,
    mail: String,
    password: String,
    onMail: (String) -> Unit,
    onPassword: (String) -> Unit,
) {
    Column(
        modifier = modifier
            .width(350.dp)
    ) {
        InputText(modifier = modifier,
            value = mail,
            onValue = onMail,
            label = stringResource(R.string.Profile_Mail_label),
            placeHolder = stringResource(R.string.Profile_Mail_spaceHolder),
            supportingNull = stringResource(id = R.string.edit_input_mail_sup)
        )
        InputPasswordLog(modifier = modifier,
            value = password,
            onValue = onPassword,
            label = stringResource(R.string.Profile_password_spaceHolder),
            placeHolder = stringResource(R.string.Profile_password_label),
            supportingNull = stringResource(id = R.string.edit_input_passw_sup)
        )
        CustomButton(
            status = !(mail == "" || password == ""),
            onClick = {
                auth.signInWithEmailAndPassword(mail, password)
                    .addOnCompleteListener(context) { task ->
                        if (task.isSuccessful) {
                            wellcomeViewModel.saveOnBoardingState(completed = true)
                            navHostController.popBackStack()
                            navHostController.navigate(MainScreen.route)
                        } else {
                            Log.w(ContentValues.TAG,
                                "signInWithEmail:failure",
                                task.exception)
                            Toast.makeText(context, R.string.auth_log_error,
                                Toast.LENGTH_SHORT).show()
                        }
                    }
            },
            text = stringResource(R.string.reg_button_Create)
        )
    }
}

@Composable
fun RegisterPartReg(
    modifier: Modifier,
    auth: FirebaseAuth,
    context: Activity,
    navHostController: NavHostController,
    wellcomeViewModel: WellcomeViewModel,
    password: String,
    onPassword: (String) -> Unit,
    mail: String,
    onMail: (String) -> Unit,
    fullname: String,
    onFullname: (String) -> Unit
){
    Column(
        modifier = modifier
            .width(350.dp)
    ) {
        InputText(modifier = modifier,
            value = fullname,
            onValue = onFullname,
            label = stringResource(R.string.Profile_realName_spaceHolder),
            placeHolder = stringResource(R.string.Profile_realName_label),
            supportingNull = stringResource(id = R.string.edit_input_fullname_sup))
        InputText(modifier = modifier,
            value = mail,
            onValue = onMail,
            label = stringResource(R.string.Profile_Mail_label),
            placeHolder = stringResource(R.string.Profile_Mail_spaceHolder),
            supportingNull = stringResource(id = R.string.edit_input_mail_sup))
        InputPasswordReg(modifier = modifier,
            value = password,
            onValue = onPassword,
            label = stringResource(R.string.Profile_password_spaceHolder),
            placeHolder = stringResource(R.string.Profile_password_label),
            supportingNull = stringResource(id = R.string.edit_input_passw_sup),
            supportingToSmall = stringResource(R.string.login_password_tosmall)
        )
        CustomButton(
            status = !(mail == "" || password == "" || fullname == "" || password.length < 3),
            onClick = {
                auth.createUserWithEmailAndPassword(mail, password)
                    .addOnCompleteListener(context) { task ->
                        if (task.isSuccessful) {
                            val db = Firebase.firestore
                            val user = hashMapOf(
                                "fullname" to fullname,
                                "mail" to mail,
                                "mobile" to "",
                                "facebook" to "",
                                "instagram" to "",
                                "twitter" to "",
                                "friends" to mapOf<String, String>(),
                                "settings" to mapOf(
                                    "facebook" to true,
                                    "instagram" to true,
                                    "twitter" to true,
                                )
                            )

                            db.collection("users").document(mail).set(user)
                                .addOnSuccessListener {
                                    Log.d(TAG,
                                        "Succes")
                                }.addOnFailureListener { e ->
                                    Log.w(TAG, "Error adding document", e)
                                }

                            wellcomeViewModel.saveOnBoardingState(completed = true)
                            navHostController.popBackStack()
                            navHostController.navigate(MainScreen.route)
                        } else {
                            Log.w(TAG,
                                "createUserWithEmail:failure",
                                task.exception)
                            Toast.makeText(context, R.string.auth_reg_error,
                                Toast.LENGTH_SHORT).show()
                        }
                    }
            },
            text = stringResource(R.string.reg_button_Create)
        )
    }
}

