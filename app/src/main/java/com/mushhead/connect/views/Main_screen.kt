package com.mushhead.connect.views


import com.mushhead.connect.R
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.mushhead.connect.components.StateList

/**
 * Funkce pro přehlednost
 * 08.02.2023 POSLEDNÍ UPDATE
 * 21.02.2023 UPDATE
 * upravoval jsem hlavně vzhled
 * 28.03.2023 UPDATE
 * HOTOVO
 */
@Composable
fun MainScreen(
    modifier: Modifier = Modifier,
    navController: NavHostController,
){
    val listHeadLine = stringResource(id = R.string.List_hed1)
    
    Column(
        modifier = modifier
            .padding(top = 5.dp, start = 10.dp, end = 10.dp)
            .fillMaxSize(),
        horizontalAlignment = Alignment.Start
    ) {
        Spacer(modifier = Modifier.height(10.dp))
        StateList(topHeadLine = listHeadLine, navController = navController)
    }
}