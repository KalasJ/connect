package com.mushhead.connect.views

import com.mushhead.connect.R
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import kotlinx.coroutines.delay
import kotlin.time.Duration.Companion.seconds

@Composable
fun FinishScreen(
    modifier: Modifier = Modifier,
    navController: NavController
){
    Column(
        modifier = modifier
            .fillMaxSize()
            .paint(
                painter = painterResource(id = R.drawable.completescreen_1),
                contentScale = ContentScale.Fit
            ),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Surface(
            modifier = modifier
                .height(30.dp)
                .width(220.dp),
            color = MaterialTheme.colorScheme.surfaceVariant.copy(alpha = (0.97f)),
            contentColor = MaterialTheme.colorScheme.onSurface,
            shadowElevation = 5.dp,
            shape = MaterialTheme.shapes.large,
            border = BorderStroke(0.5.dp,MaterialTheme.colorScheme.onSurface)
        ) {
            Text(
                modifier = modifier,
                overflow = TextOverflow.Ellipsis,
                color = MaterialTheme.colorScheme.onSurfaceVariant,
                fontFamily = MaterialTheme.typography.titleLarge.fontFamily,
                fontWeight = MaterialTheme.typography.titleLarge.fontWeight,
                lineHeight = MaterialTheme.typography.titleLarge.lineHeight,
                fontSize = MaterialTheme.typography.titleLarge.fontSize,
                text = stringResource(R.string.complete_screen_succes),
                textAlign = TextAlign.Center
            )
        }
    }
    LaunchedEffect(Unit) {
        delay(1.seconds)
        navController.popBackStack()
        navController.navigateUp()
    }
}