package com.mushhead.connect.views


import android.content.Context
import android.util.Log
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Close
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavController
import com.mushhead.connect.BarCodeAnalyser
import com.mushhead.connect.R
import com.mushhead.connect.utils.Friend
import com.mushhead.connect.viewmodels.CameraViewModel
import com.google.common.util.concurrent.ListenableFuture
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


/**
 * 14.02.2023
 * Přidal jsem společně s Add_Screenou.. třeba ještě dodělat a otestovat na reálných zařízeních. (Hlavně předělat ten Permission někam jinam)
 * UPDATE 20.02.2023
 * Předělal jsem na lepší vzhled s černým pozadím.
 * UPDATE 21.02.2023
 * Dodělán vzhled s možností se vrátit zpět (ICON).
 * UPDATE 28.03.2023
 * HOTOVO
 */
@Composable
fun CameraScreen(
    modifier: Modifier = Modifier,
    navController: NavController,
    auth: FirebaseAuth,
) {
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    val barCodeVal = remember { mutableStateOf("") }
    val toastAddYourSelf = stringResource(R.string.Toasted)
    val toastInvalidQR = stringResource(R.string.Toeasted1)
    val toastUknownError = stringResource(R.string.Toaste2)


    StatlessCameraScreen(
        modifier = modifier,
        navController = navController,
        context = context,
        lifecycleOwner = lifecycleOwner,
        auth = auth,
        barCodeVal = barCodeVal,
        toastAddYourSelf = toastAddYourSelf,
        toastInvalidQR = toastInvalidQR,
        toastUknownError = toastUknownError
    )
}


@Composable
fun StatlessCameraScreen(
    modifier: Modifier,
    navController: NavController,
    auth: FirebaseAuth,
    context: Context,
    lifecycleOwner: LifecycleOwner,
    barCodeVal: MutableState<String>,
    toastAddYourSelf: String,
    toastInvalidQR: String,
    toastUknownError: String,
) {
    Box(modifier = modifier
        .fillMaxSize()
        .background(Color.Black)
    ) {
        Body(
            modifier = modifier,
            navController = navController,
            auth = auth,
            context = context,
            lifecycleOwner = lifecycleOwner,
            barCodeVal = barCodeVal,
            toastAddYourSelf = toastAddYourSelf,
            toastInvalidQR = toastInvalidQR,
            toastUknownError = toastUknownError)
    }
}

@Composable
fun Body(
    modifier: Modifier,
    navController: NavController,
    auth: FirebaseAuth,
    context: Context,
    lifecycleOwner: LifecycleOwner,
    barCodeVal: MutableState<String>,
    toastAddYourSelf: String,
    toastInvalidQR: String,
    toastUknownError: String,
){
    Column(
        modifier = modifier
            .padding(top = 10.dp)
            .fillMaxSize(),
        horizontalAlignment = Alignment.End
    ) {
        UpperHalf(
            modifier = modifier,
            navController = navController
        )
        LowerHalf(
            modifier = modifier,
            navController = navController,
            auth = auth,
            context = context,
            lifecycleOwner = lifecycleOwner,
            barCodeVal = barCodeVal,
            toastAddYourSelf = toastAddYourSelf,
            toastInvalidQR = toastInvalidQR,
            toastUknownError = toastUknownError
        )
    }
}

@Composable
fun UpperHalf(
    modifier: Modifier,
    navController: NavController,
) {
    Row(
        modifier = modifier
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.Start
    ) {
        Text(
            modifier = modifier
                .align(Alignment.CenterVertically)
                .padding(start = 15.dp),
            fontStyle = MaterialTheme.typography.titleLarge.fontStyle,
            fontWeight = MaterialTheme.typography.titleLarge.fontWeight,
            fontSize = MaterialTheme.typography.titleLarge.fontSize,
            color = Color.White,
            text = stringResource(R.string.camera_qr_scan),
        )
        Spacer(modifier = Modifier.weight(1f))
        BackIcon(
            modifier = modifier
                .padding(10.dp)
                .alignByBaseline(),
            navController = navController
        )
    }
}

@Composable
fun LowerHalf(
    modifier: Modifier,
    navController: NavController,
    cameraViewModel: CameraViewModel = hiltViewModel(),
    auth: FirebaseAuth,
    context: Context,
    lifecycleOwner: LifecycleOwner,
    barCodeVal: MutableState<String>,
    toastAddYourSelf: String,
    toastInvalidQR: String,
    toastUknownError: String,
) {
    var preview by remember { mutableStateOf<Preview?>(null) }

    Column(
        modifier = modifier
            .fillMaxSize(),
        verticalArrangement = Arrangement.Center
    ) {
        AndroidView(
            factory = { AndroidViewContext ->
                PreviewView(AndroidViewContext).apply {
                    this.scaleType = PreviewView.ScaleType.FILL_CENTER
                    layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                    )
                    implementationMode = PreviewView.ImplementationMode.COMPATIBLE
                }
            },
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 10.dp, bottom = 80.dp)
                .clip(shape = RoundedCornerShape(15.dp)),
            update = { previewView ->
                val cameraSelector: CameraSelector = CameraSelector.Builder()
                    .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                    .build()
                val cameraExecutor: ExecutorService = Executors.newSingleThreadExecutor()
                val cameraProviderFuture: ListenableFuture<ProcessCameraProvider> =
                    ProcessCameraProvider.getInstance(context)

                cameraProviderFuture.addListener({
                    preview = Preview.Builder().build().also {
                        it.setSurfaceProvider(previewView.surfaceProvider)
                    }
                    val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
                    val barcodeAnalyser = BarCodeAnalyser { barcodes ->
                        barcodes.forEach { barcode ->
                            barcode.rawValue?.let { barcodeValue ->
                                barCodeVal.value = barcodeValue

                                val profile = cameraViewModel.bardcodeToUser(barcodeValue)
                                //Log.d("PROFILE", "${profile[1]}")
                                if (profile.size == 3 && profile[1] != "" && profile[2] != "" && cameraViewModel.isValidEmail(
                                        profile[0])
                                ) {
                                    val friend = Friend(
                                        mail = profile[0],
                                        lat = profile[1],
                                        long = profile[2]
                                    )

                                    if (auth.currentUser!!.email != "") {
                                        if (auth.currentUser!!.email != profile[0]) {
                                            val db = Firebase.firestore
                                            val userRef = db.collection("users")
                                                .document(auth.currentUser!!.email!!)

                                            userRef.update("friends.${
                                                profile[0].replace(".",
                                                    "-")
                                            }", friend)
                                            navController.popBackStack()
                                            navController.navigate(com.mushhead.connect.utils.CompleteScreen.route)
                                        } else {
                                            val toast = Toast.makeText(context,
                                                toastAddYourSelf,
                                                Toast.LENGTH_LONG)

                                            toast.show()
                                            navController.popBackStack()
                                        }
                                    } else {
                                        val toast = Toast.makeText(context,
                                            toastUknownError,
                                            Toast.LENGTH_LONG)

                                        toast.show()
                                        navController.popBackStack()
                                    }
                                } else {
                                    val toast =
                                        Toast.makeText(context, toastInvalidQR, Toast.LENGTH_LONG)

                                    toast.show()
                                    navController.popBackStack()
                                }
                            }
                        }
                    }
                    val imageAnalysis: ImageAnalysis = ImageAnalysis.Builder()
                        .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                        .build()
                        .also {
                            it.setAnalyzer(cameraExecutor, barcodeAnalyser)
                        }
                    try {
                        cameraProvider.unbindAll()
                        cameraProvider.bindToLifecycle(
                            lifecycleOwner,
                            cameraSelector,
                            preview,
                            imageAnalysis
                        )
                    } catch (e: Exception) {
                        Log.d("TAG", "CameraPreview: ${e.localizedMessage}")
                    }
                }, ContextCompat.getMainExecutor(context))
            }
        )
    }
}

@Composable
fun BackIcon(
    modifier: Modifier,
    navController: NavController,
) {
    IconButton(
        modifier = modifier,
        onClick = {
            if (navController.previousBackStackEntry != null) {
                navController.popBackStack()
            }
        },
    ) {
        Icon(
            tint = Color.White,
            imageVector = Icons.Outlined.Close,
            contentDescription = "To previous screen"
        )
    }
}