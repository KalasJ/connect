package com.mushhead.connect.views

import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.text.isDigitsOnly
import androidx.navigation.NavController
import com.mushhead.connect.R
import com.mushhead.connect.components.CustomButton
import com.mushhead.connect.components.InputRow
import com.mushhead.connect.components.InputRowEditScreen
import com.mushhead.connect.components.InputRowMobile
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


/**
 * 06.03.2023 CREATE
 * Vytvořil jsem samostatnou screen pro editaci profilu a trochu upravil kod.
 * 12.03.2023 UPDATE
 * Obrovské přepracování, vytvočil jsem server a již nepotřebuji nynější databázy.
 * 28.03.2023 UPDATE
 * HOTOVO
 */
@Composable
fun EditScreen(
    modifier: Modifier = Modifier,
    navController: NavController,
    auth: FirebaseAuth,
) {
    //DATABASE CALL FOR USER INFORMATIONS
    val db = Firebase.firestore
    val docRef = db.collection("users").document(auth.currentUser!!.email!!)
    var name by remember { mutableStateOf("") }
    var mobile by remember { mutableStateOf("") }
    var facebook by remember { mutableStateOf("") }
    var instagram by remember { mutableStateOf("") }
    var twitter by remember { mutableStateOf("") }

    //DATABASE CALL FOR USER INFORMATIONS
    var readOnce by remember { mutableStateOf(true) }
    if (readOnce) {
        docRef.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val document = task.result
                name = document.data!!["fullname"] as String
                mobile = document.data!!["mobile"] as String
                facebook = document.data!!["facebook"] as String
                instagram = document.data!!["instagram"] as String
                twitter = document.data!!["twitter"] as String
                Log.d("TT", "Cached document data: ${document?.data}")
                readOnce = false
            } else {
                Log.d("TT", "Cached get failed: ", task.exception)
            }
        }
    }

    StatlessEditScreen(
        modifier = modifier,
        navController = navController,
        db = db,
        auth = auth,
        name = name,
        mobile = mobile,
        facebook = facebook,
        instagram = instagram,
        twitter = twitter,
        onName = { name = it },
        onMobile = { mobile = it },
        onFacebook = { facebook = it },
        onInstagram = { instagram = it },
        onTwitter = { twitter = it },
    )
}

@Composable
fun StatlessEditScreen(
    modifier: Modifier,
    navController: NavController,
    db: FirebaseFirestore,
    auth: FirebaseAuth,
    name: String,
    mobile: String,
    facebook: String,
    instagram: String,
    twitter: String,
    onName: (String) -> Unit,
    onMobile: (String) -> Unit,
    onFacebook: (String) -> Unit,
    onInstagram: (String) -> Unit,
    onTwitter: (String) -> Unit,
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Body(
            modifier = modifier,
            navController = navController,
            db = db,
            auth = auth,
            name = name,
            mobile = mobile,
            facebook = facebook,
            instagram = instagram,
            twitter = twitter,
            onName = onName,
            onMobile = onMobile,
            onFacebook = onFacebook,
            onInstagram = onInstagram,
            onTwitter = onTwitter)
    }
}

@Composable
fun Body(
    modifier: Modifier,
    navController: NavController,
    db: FirebaseFirestore,
    auth: FirebaseAuth,
    name: String,
    mobile: String,
    facebook: String,
    instagram: String,
    twitter: String,
    onName: (String) -> Unit,
    onMobile: (String) -> Unit,
    onFacebook: (String) -> Unit,
    onInstagram: (String) -> Unit,
    onTwitter: (String) -> Unit,
) {
    Surface(
        modifier = modifier
            .wrapContentSize()
            .width(350.dp),
        color = MaterialTheme.colorScheme.surfaceVariant.copy(alpha = (0.97f)),
        contentColor = MaterialTheme.colorScheme.onSurface,
        shadowElevation = 5.dp,
        shape = MaterialTheme.shapes.large,
        border = BorderStroke(0.5.dp, MaterialTheme.colorScheme.onSurface)
    ) {
        Column(
            modifier = modifier
                .padding(10.dp)
        ) {
            UserInformation(
                modifier = modifier,
                name = name,
                mobile = mobile,
                onMobile = onMobile,
                onName = onName,
            )
            UserSocialInformation(
                modifier = modifier,
                facebook = facebook,
                onFacebook = onFacebook,
                instagram = instagram,
                onInstagram = onInstagram,
                twitter = twitter,
                onTwitter = onTwitter
            )
            CustomButton(
                modifier = modifier,
                onClick = {
                    val washingtonRef = db.collection("users").document(auth.currentUser!!.email!!)
                    washingtonRef
                        .update("fullname",
                            name,
                            "mobile",
                            mobile,
                            "facebook",
                            facebook,
                            "instagram",
                            instagram,
                            "twitter",
                            twitter)
                        .addOnSuccessListener {
                            Log.d("Update",
                                "DocumentSnapshot successfully updated!")
                        }
                        .addOnFailureListener { e -> Log.w("Update", "Error updating document", e) }
                    navController.popBackStack()
                },
                status = name != "",
                text = stringResource(R.string.edit_button_save)
            )
        }
    }
}


@Composable
fun UserInformation(
    modifier: Modifier,
    name: String,
    mobile: String,
    onName: (String) -> Unit,
    onMobile: (String) -> Unit,
) {

    Column(
        horizontalAlignment = Alignment.Start,
        modifier = modifier
            .padding(10.dp)
    ) {
        Column {
            Text(
                modifier = modifier
                    .padding(bottom = 10.dp, start = 5.dp),
                fontStyle = MaterialTheme.typography.titleMedium.fontStyle,
                fontWeight = MaterialTheme.typography.titleMedium.fontWeight,
                fontSize = MaterialTheme.typography.titleMedium.fontSize,
                text = stringResource(id = R.string.Profile_UserColumn_label)
            )
            InputRow(
                modifier = modifier,
                value = name,
                onValueChange = { onName(it) },
                label = stringResource(id = R.string.Profile_realName_label),
                placeHolder = stringResource(id = R.string.Profile_realName_spaceHolder),
                supporting =
                if (name == "") {
                    stringResource(id = R.string.edit_input_fullname_sup)
                } else {
                  ""
                },
            )
            InputRowMobile(
                modifier = modifier,
                value = mobile,
                onValueChange = {
                    if (mobile.length < 9) {
                        onMobile(it)
                    } else {
                        onMobile(it.dropLast(1))
                    }
                },
                label = stringResource(id = R.string.Profile_Number_label),
                placeHolder = stringResource(id = R.string.Profile_Number_spaceHolder),
                supporting =
                if (!mobile.isDigitsOnly()) {
                    stringResource(id = R.string.Toust_mobile_number)
                } else {
                    if (mobile.length > 8) {
                        stringResource(id = R.string.Toust_mobile_long)
                    } else {
                        ""
                    }
                },
            )
        }
    }
}

@Composable
fun UserSocialInformation(
    modifier: Modifier,
    facebook: String,
    instagram: String,
    twitter: String,
    onFacebook: (String) -> Unit,
    onInstagram: (String) -> Unit,
    onTwitter: (String) -> Unit,
) {
    Column(
        horizontalAlignment = Alignment.Start,
        modifier = modifier
            .padding(10.dp)
    ) {
        Text(
            modifier = modifier
                .padding(bottom = 10.dp, start = 5.dp),
            fontStyle = MaterialTheme.typography.titleMedium.fontStyle,
            fontWeight = MaterialTheme.typography.titleMedium.fontWeight,
            fontSize = MaterialTheme.typography.titleMedium.fontSize,
            text = stringResource(id = R.string.Profile_SocialColumn_label)
        )
        InputRowEditScreen(
            modifier = modifier,
            value = facebook,
            onValueChange = onFacebook,
            label = stringResource(id = R.string.Profile_facebook_label),
            placeHolder = stringResource(id = R.string.Profile_facebook_spaceHolder)
        )
        InputRowEditScreen(
            modifier = modifier,
            value = instagram,
            onValueChange = onInstagram,
            label = stringResource(id = R.string.Profile_instagram_label),
            placeHolder = stringResource(id = R.string.Profile_instagram_spaceHolder)
        )
        InputRowEditScreen(
            modifier = modifier,
            value = twitter,
            onValueChange = onTwitter,
            label = stringResource(id = R.string.Profile_twitter_label),
            placeHolder = stringResource(id = R.string.Profile_twitter_spaceHolder)
        )
    }
}