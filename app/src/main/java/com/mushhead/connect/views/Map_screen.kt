package com.mushhead.connect.views

import android.Manifest
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.hilt.navigation.compose.hiltViewModel
import com.mushhead.connect.R
import com.mushhead.connect.utils.Friend
import com.mushhead.connect.utils.User
import com.mushhead.connect.viewmodels.UsersViewModel
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.*

/**
 * 13.02.2023
 * Přidal jsem basic mapu na budoucí zpracování.
 * 26.02.2023 UPDATE
 * Upravil jsem mapu do hezkčího vzhledu a zfunčknil.
 * Třeba ještě alespoň udělat custom markers
 * 27.02.2023 UPDATE
 * Mněnil jsem proměnné, abych dosáhl správných observrů změn hodnot přes liveData. (další fixy ve viewModelech)
 * 28.03.2023 UPDATE
 * HOTOVO
 */
@Composable
fun MapScreen(
    modifier: Modifier = Modifier,
    usersViewModel: UsersViewModel = hiltViewModel(),
    lat: String = "0.0",
    lon: String = "0.0",
) {
    //permission
    val context = LocalContext.current
    val permissionL = Manifest.permission.ACCESS_FINE_LOCATION
    val permissionCheckResult = ContextCompat.checkSelfPermission(context, permissionL)

    //Nastavení pro mapu
    val uiSettings by remember { mutableStateOf(MapUiSettings()) }
    var properties by remember { mutableStateOf(MapProperties(mapType = MapType.NORMAL, isMyLocationEnabled = false)) }
    if(permissionCheckResult != -1){
        properties = MapProperties(mapType = MapType.NORMAL, isMyLocationEnabled = true)
    }
    val latLong = LatLng(lat.toDouble(), lon.toDouble())
    val coordinates = CameraPositionState(position = CameraPosition.fromLatLngZoom(latLong, 11f))
    val showOnce = rememberSaveable { mutableStateOf(true) }
    //Log.d("POSITION", "$lat $lon")

    if(latLong.latitude == 50.073658 && latLong.longitude == 14.418540 && showOnce.value){
        val toast = Toast.makeText(context, stringResource(R.string.map_toasted_1), Toast.LENGTH_LONG)
        toast.show()
        showOnce.value = false
    }

    StatlessMap(
        modifier = modifier
            .fillMaxSize()
            .padding(5.dp)
            .clip(RoundedCornerShape(10.dp)),
        friends = usersViewModel.owner.value.friends,
        friendsDetail = usersViewModel.friends.value,
        uiSettings = uiSettings,
        properties = properties,
        cameraPositionState = coordinates
    )

}



@Composable
fun StatlessMap(
    modifier: Modifier,
    uiSettings: MapUiSettings,
    properties: MapProperties,
    friends: Map<String, Friend>,
    friendsDetail: List<User>,
    cameraPositionState: CameraPositionState
) {
    Column(
        modifier = modifier
            .fillMaxSize()
    ) {
        Box(
            modifier = modifier.fillMaxSize(1f)
        ) {
            GoogleMap(
                modifier = modifier.matchParentSize(),
                properties = properties,
                uiSettings = uiSettings,
                cameraPositionState = cameraPositionState
            ) {
                for (friend in friends) {
                    val friendDetail = friendsDetail.firstNotNullOfOrNull { detail -> detail.takeIf { it.mail == friend.key.replace("-",".")}}
                    if (friend.value.lat != "e" && friend.value.long != "e") {
                        val coordinates = LatLng(friend.value.lat.toDouble(), friend.value.long.toDouble())
                        Marker(
                            state = MarkerState(coordinates),
                            title = friendDetail?.fullname ?: "UKNOWN"
                        )
                    }
                }
            }
        }
    }
}
