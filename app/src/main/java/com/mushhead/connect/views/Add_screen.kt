package com.mushhead.connect.views


import android.Manifest
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Context
import android.content.pm.PackageManager
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.BorderStroke
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.draw.paint
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.core.content.ContextCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.mushhead.connect.R
import com.mushhead.connect.components.CircularProgress
import com.mushhead.connect.components.CustomButton
import com.mushhead.connect.components.CustomDialogPermission
import com.mushhead.connect.utils.AddScreen
import com.mushhead.connect.viewmodels.AddViewModel
import com.mushhead.connect.viewmodels.MainViewModel
import com.mushhead.connect.viewmodels.LocationViewModel
import com.mushhead.connect.viewmodels.UsersViewModel
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import com.simonsickle.compose.barcodes.Barcode
import com.simonsickle.compose.barcodes.BarcodeType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * 14.02.2023
 * Dokončil jsem Add_screen, chtěl jsem tu technologii pro předání vést přes NFC, ale bohužel jsem stejně jak u FB API narazil na nedostatek informací které bych mohl využít pro implementaci
 * jinak řečeno, dělalo se to přes starý postupy s XML a já nevím jak to udělat.. vrátím se k tomu nakonec stejně jak k těm APInám.
 * Mám funkční generováné QR kodu + čtení QR kodu.. (ještě budu upravovat)
 * UPDATE 20.02.2023
 * Hrál jsem si s přístupy a upravoval kameru.
 * UPDATE 23.02.2023
 * ÚSPĚCH ! Podailo se mi rozjet zjištování lokace a již tomu i rozumím.
 * Na této screeně momentálně zjištuji (observuji) lokaci a již ji vkládám do QR kodu, který se přegenerovává při změně lokace (díky liveData a observu).
 * K té příležitosti jsem upravil i funkce ve ViewModelu, aby fungovali s lokací.
 * UPDATE 28.03.2023
 * HOTOVO
 */

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun AddScreen(
    navController: NavController,
    onClickSeeCamera: () -> Unit,
    addViewModel: AddViewModel = hiltViewModel(),
    usersViewModel: UsersViewModel = hiltViewModel(),
    locationViewModel: LocationViewModel = hiltViewModel(),
) {

    //OWNER
    val owner = usersViewModel.owner.value
    var lat by remember { mutableStateOf("") }
    var lon by remember { mutableStateOf("") }

    //OTHERS
    val openDialog = remember { mutableStateOf(false) }
    val hidenButton = rememberSaveable { mutableStateOf(true) }
    val scope = rememberCoroutineScope()
    var loading by remember { mutableStateOf(true) }

    //LOCATION
    val lifecycleOwner = LocalLifecycleOwner.current
    val context = LocalContext.current

    //Permissions:
    val locationPermissionState = rememberPermissionState(ACCESS_FINE_LOCATION)
    val cameraPermissionState = rememberPermissionState(Manifest.permission.CAMERA)
    val permissionL = ACCESS_FINE_LOCATION
    val permissionC = Manifest.permission.CAMERA

    //LAUNCHER - PERMISION FOR LOCATION
    val launcherL = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) {
            hidenButton.value = false
            navController.navigate(AddScreen.route)
            locationViewModel.getLocationLiveData().observe(lifecycleOwner) {
                lat = (it.lat.toString())
                lon = (it.lon.toString())
            }
        } else {
            locationPermissionState.permissionRequested
            openDialog.value = true
        }
    }

    //LAUNCHER - PERMISSION FOR CAMERA
    val launcherC = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) {
            onClickSeeCamera()
        } else {
            cameraPermissionState.permissionRequested
            openDialog.value = true
        }
    }

    //LOCATION OBSERVE UPDATES
    val permissionCheckResult = ContextCompat.checkSelfPermission(context, permissionL)
    locationViewModel.getLocationLiveData().observe(lifecycleOwner) {
        lat = (it.lat.toString())
        lon = (it.lon.toString())
    }

    if(lat == ""|| lon == "") {
        lat = "e"
        lon = "e"
    }

    //DIALOG, which pops up when permission has not been granted and warns about it.
    if (openDialog.value) {
        CustomDialogPermission { openDialog.value = !openDialog.value }
    }

    val information = addViewModel.userInformationConnector(owner.mail, lat,lon)
    //Log.d("USER", information)

    StatlessAddScreen(
        onClickSeeCamera = onClickSeeCamera,
        context = context,
        scope = scope,
        information = information,
        loading = loading,
        onLoading = {loading = it },
        permissionL = permissionL,
        permissionC = permissionC,
        launcherL = launcherL,
        launcherC = launcherC,
        permissionCheckResult = permissionCheckResult
    )
}

@Composable
fun StatlessAddScreen(
    modifier: Modifier = Modifier,
    onClickSeeCamera: () -> Unit,
    context: Context,
    scope: CoroutineScope,
    permissionL: String,
    permissionC: String,
    launcherL: ManagedActivityResultLauncher<String, Boolean>,
    launcherC: ManagedActivityResultLauncher<String, Boolean>,
    permissionCheckResult: Any,
    information: String,
    loading: Boolean,
    onLoading: (Boolean) -> Unit,
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(5.dp)
            .paint(
                painter = painterResource(id = R.drawable.addscreen_1),
                contentScale = ContentScale.Fit
            ),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Surface(
            modifier = modifier
                .size(600.dp)
                .padding(start = 20.dp, end = 20.dp),
            color = MaterialTheme.colorScheme.surfaceVariant.copy(alpha = (0.97f)),
            contentColor = MaterialTheme.colorScheme.onSurface,
            shadowElevation = 5.dp,
            shape = MaterialTheme.shapes.large,
            border = BorderStroke(0.5.dp,MaterialTheme.colorScheme.onSurface)
        ) {
            Body(
                onClickSeeCamera = onClickSeeCamera,
                context = context,
                scope = scope,
                permissionL = permissionL,
                permissionC = permissionC,
                launcherL = launcherL,
                launcherC = launcherC,
                permissionCheckResult = permissionCheckResult,
                onLoading = onLoading,
                loading = loading,
                information = information)
        }
    }
}

@Composable
fun Body(
    modifier: Modifier = Modifier,
    onClickSeeCamera: () -> Unit,
    mainViewModel: MainViewModel = hiltViewModel(),
    context: Context,
    scope: CoroutineScope,
    permissionL: String,
    permissionC: String,
    launcherL: ManagedActivityResultLauncher<String, Boolean>,
    launcherC: ManagedActivityResultLauncher<String, Boolean>,
    permissionCheckResult: Any,
    onLoading: (Boolean) -> Unit,
    loading: Boolean,
    information: String,
){
    Column(
        modifier = modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        UpperHalf(
            modifier = modifier.weight(3f),
            mainViewModel = mainViewModel,
            context = context,
            scope = scope,
            permissionL = permissionL,
            launcherL = launcherL,
            permissionCheckResult = permissionCheckResult,
            onLoading = onLoading,
            loading = loading,
            information = information)
        Spacer(modifier = Modifier.height(10.dp))
        LowerHalf(
            modifier = modifier.weight(1f),
            onClickSeeCamera = onClickSeeCamera,
            mainViewModel = mainViewModel,
            context = context,
            permissionC = permissionC,
            launcherC = launcherC)
    }
}

@Composable
fun LowerHalf(
    modifier: Modifier,
    onClickSeeCamera: () -> Unit,
    mainViewModel: MainViewModel,
    context: Context,
    permissionC: String,
    launcherC: ManagedActivityResultLauncher<String, Boolean>
){
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        //QR SCANER
        Text(
            text = stringResource(R.string.add_text_newPerson),
            fontSize = MaterialTheme.typography.titleMedium.fontSize,
            fontWeight = MaterialTheme.typography.titleMedium.fontWeight,
            fontFamily = MaterialTheme.typography.titleMedium.fontFamily
        )
        androidx.compose.material.Divider(
            modifier = Modifier
                .padding(5.dp),
            thickness = 0.5.dp,
            color = Color.Gray
        )
        CustomButton(
            onClick = {
                mainViewModel.checkAndRequestCameraPermission(onClickSeeCamera,
                    context,
                    permissionC,
                    launcherC)
            },
            text = stringResource(R.string.add_button_openCamera),
        )
    }
}

@Composable
fun UpperHalf(
    modifier: Modifier,
    mainViewModel: MainViewModel,
    context: Context,
    scope: CoroutineScope,
    permissionL: String,
    launcherL: ManagedActivityResultLauncher<String, Boolean>,
    permissionCheckResult: Any,
    onLoading: (Boolean) -> Unit,
    loading: Boolean,
    information: String,
){
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        //QR READER
        Text(
            modifier = Modifier
                .padding(bottom = 5.dp),
            fontSize = MaterialTheme.typography.titleMedium.fontSize,
            fontWeight = MaterialTheme.typography.titleMedium.fontWeight,
            fontFamily = MaterialTheme.typography.titleMedium.fontFamily,
            text = stringResource(R.string.add_QR_Scan)
        )
        androidx.compose.material.Divider(
            modifier = Modifier
                .padding(5.dp),
            thickness = 0.5.dp,
            color = Color.Gray
        )
        Spacer(modifier = Modifier.size(10.dp))
        if (BarcodeType.QR_CODE.isValueValid(information)) {
            scope.launch {
                delay(1000)
                onLoading(false)
            }
            CircularProgress(modifier = modifier, isDisplayed = loading)
            AnimatedVisibility(visible = !loading) {
                Barcode(
                    modifier = modifier
                        .size(300.dp),
                    showProgress = false,
                    resolutionFactor = 10, // Optionally, increase the resolution of the generated image
                    type = BarcodeType.QR_CODE, // pick the type of barcode you want to render
                    value = information
                )
            }
        } else {
            Text(stringResource(R.string.add_QR_invalid))
        }
        if (permissionCheckResult != PackageManager.PERMISSION_GRANTED) {
            CustomButton(
                onClick = {
                    mainViewModel.checkAndRequestLocationPermission(
                        context,
                        permissionL,
                        launcherL)
                },
                text = stringResource(R.string.add_button_permission),
            )
        }
    }
}

