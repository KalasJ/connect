package com.mushhead.connect.viewmodels

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mushhead.connect.repository.UsersRepository
import com.mushhead.connect.utils.Resource
import com.mushhead.connect.utils.User
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(
    private val usersRepository: UsersRepository,
) : ViewModel() {

    val loading = mutableStateOf(false)
    var empty = mutableStateOf(true)


    // List of all users
    private var _users = mutableStateOf<List<User>>(emptyList())
    val users: State<List<User>> = _users


    // Owner
    private var _owner = mutableStateOf(User())
    val owner: State<User> = _owner

    // Friends
    private var _friends = mutableStateOf<List<User>>(emptyList())
    val friends: State<List<User>> = _friends

    fun deleteFriend(mail: String) {
        viewModelScope.launch(Dispatchers.IO) {
            usersRepository.deleteUser(mail)
        }
    }

    fun updateSettings(setting: String, option: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            usersRepository.updateSettings(setting, option)
        }
    }

    init {
        // CALL OWNER OF THE APP
        // The function will be called when the viewModel gets called
        usersRepository.getOwnerRealTime()
            // onEach will trigger whenever a new value is retrieved
            .onEach { resource ->
                when (resource) {
                    is Resource.Error -> {
                        /* TODO: Handle the error */
                    }

                    is Resource.Success -> {
                        _owner.value = resource.data!![0]
                        //FRIENDS
                        empty.value = _owner.value.friends.isEmpty()
                        loading.value = !empty.value
                        usersRepository.getOwnersFriends(_owner.value.friends)
                            .onEach { friend ->
                                when (friend) {
                                    is Resource.Error -> {
                                        /* TODO: Handle the error */
                                        loading.value = false
                                    }

                                    is Resource.Success -> {
                                        _friends.value = friend.data!!
                                        loading.value = false
                                    }

                                }
                            }
                            .launchIn(viewModelScope)
                    }
                }
            }.launchIn(viewModelScope)
    }
}