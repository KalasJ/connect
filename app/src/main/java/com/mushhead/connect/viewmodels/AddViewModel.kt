package com.mushhead.connect.viewmodels

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AddViewModel @Inject constructor() : ViewModel() {

    fun userInformationConnector(
        mail: String,
        lan: String,
        lon: String,
    ): String {
        return "$mail/$lan/$lon"
    }

}