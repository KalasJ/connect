package com.mushhead.connect.viewmodels

import android.content.Context
import android.content.pm.PackageManager
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


/**
 * 08.02.2023 UPDATE
 * Konecně se mi podařilo rozjet viewModel, problém byl v tom, že jsem nevěděl jak jej inicializovat až na místě kde jsem jej potřeboval použít.
 * Obsahuje jednoduché požadavky na databázy s dvěmi proměnými které používám venku v listu
 * 23.02.2023 UPDATE
 * Tento týden jsem přidal dva handlery na vyžadování permissions.
 * Dnes jsem přidal pár chytrých funkcí pro práci a kontrolu s QR kodem a s databází
 * 28.02.2023 UPDATE
 * HOTOVO
 */
@HiltViewModel
class MainViewModel @Inject constructor() : ViewModel() {

    //Permission HANDLER
    fun checkAndRequestLocationPermission(
        context: Context,
        permission: String,
        launcher: ManagedActivityResultLauncher<String, Boolean>,
    ) {
        val permissionCheckResult = ContextCompat.checkSelfPermission(context, permission)
        if (permissionCheckResult != PackageManager.PERMISSION_GRANTED) {
            launcher.launch(permission)
        }
    }

    //Permission HANDLER
    fun checkAndRequestCameraPermission(
        onClickGoSomewhere: () -> Unit,
        context: Context,
        permission: String,
        launcher: ManagedActivityResultLauncher<String, Boolean>,
    ) {
        val permissionCheckResult = ContextCompat.checkSelfPermission(context, permission)
        if (permissionCheckResult == PackageManager.PERMISSION_GRANTED) {
            onClickGoSomewhere()
        } else {
            launcher.launch(permission)
        }
    }
}