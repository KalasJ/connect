package com.mushhead.connect.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import com.mushhead.connect.classes.LocationsLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * 23.02.2023
 * Nový viewModel pro zfunkčnění lokace
 */
@HiltViewModel
class LocationViewModel @Inject constructor(application: Application) : ViewModel() {

    private val LocationLiveData = LocationsLiveData(application)
    fun getLocationLiveData() = LocationLiveData

}