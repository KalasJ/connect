package com.mushhead.connect.viewmodels

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor() : ViewModel() {

    fun emptyCheck(value: String): Boolean{
        if(value == "e" || value == ""){
            return false
        }
        return true
    }
}