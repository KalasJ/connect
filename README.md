# Projekt Connect

Tento projekt je osobním vstupem do vývoje mobilních aplikací Android.

Projekt je součástí bakalářské práce vypracované na Mendelově Univerzitě. 

Odkaz na zpracovanou bc. práci: https://drive.google.com/file/d/1EvgmRaHN6UziuMAIb4_HGaGdRQHliCBL/view?usp=sharing
Odakz na celý projekt: https://drive.google.com/file/d/1X8huwSiFtOh9_pbZ4bWewvTNAvsBVnbx/view?usp=sharing

## Popis
Cílem téhoto projektu bylo vytvoření moderní mobilní aplikace pro seznamování lidí při osobním kontaktu tak, aby splňovala veškeré normy a potřeby jak dnešních standartů, tak i jednotlivých uživatelů. Hlavním úkolem aplikace je předání předem vybraných osobních informací mezi dvěma uživateli za bezpečnějšího průběhu než při verbální nebo jiné komunikaci.

## Využité technologie a postupy
Firebase, Compose, Dagger and Hilt, QR generator/reader, Google Map, Material3

## Grafické podklady

<img src="IMG/edit.jpg" width="200">
<img src="IMG/empty.jpg" width="200">
<img src="IMG/list.jpg" width="200">
<img src="IMG/load.jpg" width="200">
<img src="IMG/map.jpg" width="200">
